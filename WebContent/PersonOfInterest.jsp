<%@page import="ch.ethz.inf.dbproject.util.UserManagement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>
<% final User user = (User) session.getAttribute(UserManagement.SESSION_USER); %>
<h1>Person of Interest</h1>

<hr/>

<%= session.getAttribute("personOfInterestTable") %>

<hr/>
<% if (user != null) { %>
	<a href="ModifyPersonOfInterest?id=<%=request.getParameter("id")%>">
 			<button>Modify Person</button>
	</a>
	<form action="PersonOfInterest" method="post">
		<input type="hidden" name="id" value="<%= request.getParameter("id") %>" />
		<br />
		Add Note
		<br />
		<input type="hidden" name="action" value="add_note" />
		<textarea rows="4" cols="50" name="note"></textarea>
		<br />

		<input type="submit" value="Submit Note" />
		<hr/>


	</form>
	<% }%>

<h2>Linked Cases</h2>

<%= session.getAttribute("linkedCasesTable") %>

<hr/>

<h2>Convictions</h2>

<%= session.getAttribute("convictionsTable") %>

<hr/>

<h2>Notes</h2>

<%= session.getAttribute("notesTable") %>

<hr/>

<%@ include file="Footer.jsp" %>