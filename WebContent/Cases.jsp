<%@page import="ch.ethz.inf.dbproject.util.UserManagement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>
<% final User user = (User) session.getAttribute(UserManagement.SESSION_USER); %>

<h1>

<% String filter = request.getParameter("filter");
   String category = request.getParameter("category");
		if (filter == null && category == null) {			
			// If no filter is specified, then we display nothing!
		} else if (category != null) {
		%>
		<%=category %>
		<% 	
		} else if (filter != null) {
			%>
			<%=filter %>
		<%} %> 
		Cases</h1>
<hr/>

<%= session.getAttribute("cases") %>

<hr/>

<% if (user != null) {
	 if(request.getParameter("category") != null) {%> 
<a href="AddCase?category=<%=request.getParameter("category")%>">
<button>Add case in this category</button>
</a>
<% } } %>
<%@ include file="Footer.jsp" %>