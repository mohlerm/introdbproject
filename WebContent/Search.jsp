<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h1>Search</h1>

<hr/>

<form method="get" action="Search">
<div>
	<input type="hidden" name="filter" value="description" />
	Search for case in Description or user who created the case:
	<br />
	<input type="text" name="description" />
	
	<select name="status" id="statusId">
	<option value="Any status">Any status</option>
		<% for(Status s : DatastoreInterfaceSingleton.getInstance().getAllStatus()) { %>
		  		<option value="<%=s.getStatus()%>"><%= s.getStatus() %></option>
		<% } %>
	</select>
	<input type="submit" value="Search" title="Search by Description, Person Of Interest and user who created the case" />
</div>
</form>

<hr/>

<form method="get" action="Search">
<div>
	<input type="hidden" name="filter" value="poi" />
	Search Person of Interest:
	<br />
	<input type="text" name="person" />
	<input type="submit" value="Search" title="Search Person of interest" />
</div>
</form>

<hr/>

<form method="get" action="Search">
<div>
	<input type="hidden" name="filter" value="conviction" />
	Search Conviction:
	<br />
	<input type="text" name="name" />
	<select name="type" id="typeId">
	<option value="Any type">Any type</option>
		<% List<Category> categoryList = DatastoreInterfaceSingleton.getInstance().getAllCategories();
		for(Category c : categoryList) { 
		if(c.getParent() != null) {
			 if(request.getParameter("category") != null && request.getParameter("category").equals(c.getName())) { 
				%>
				<option value="<%=c.getName()%>" selected >&nbsp&nbsp<%=c.getName()%></option>
			<% } else {%>
	  			<option value="<%=c.getName()%>" >&nbsp&nbsp<%=c.getName()%></option>
	  			<% } %>
	  	<% } else { %>
	  	<option value="<%=c.getName()%>" disabled><%=c.getName()%></option>
		<% } } %>
	</select>
	Convicted during this date (can be empty)
	<input type="date" name="convictedBy" value="" />
	<input type="submit" value="Search" title="Search Convictions" />
</div>
</form>

<hr/>
<%if(session.getAttribute("result")!=null){%>
	<%=session.getAttribute("result")%>
<%}%>

<hr/>

<%@ include file="Footer.jsp" %>