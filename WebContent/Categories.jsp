<%@page import="ch.ethz.inf.dbproject.CategoriesServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h2>Categories</h2>

<% 

	// User is not logged in. Only display the categories:
%>

<%=  session.getAttribute("categoriesTable") %>

<%
	
if ((Boolean) session.getAttribute(CategoriesServlet.SESSION_USER_LOGGED_IN)) {
	// User is logged in. Display the new category form
%>
    <h3>Add a new sub category</h3>
	<form action="Categories" method="get">
	<input type="hidden" name="action" value="addCategory" />
	<table>
		<tr>
			<th>Category</th>
			<td><select name="topCategory" id="topCategoryId">
			<% List<Category> categoryList = (List<Category>) session.getAttribute("categoryList"); %>
			<% for(Category c : categoryList) { %>
			<% if(c.getParent() == null) { %>
		  	<option value="<%=c.getName()%>"><%=c.getName()%></option>
			<% } } %></select></td>
		</tr>
		<tr>
			<th>Title</th>
			<td><input type="text" name="subCategory" value="" /></td>
		</tr>
		<tr>
			<th colspan="5">
				<input type="submit" value="Add" />
			</th>
		</tr>
	</table>
	</form>


<%
}
%>

<%@ include file="Footer.jsp" %>