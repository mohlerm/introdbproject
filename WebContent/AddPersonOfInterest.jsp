<%@page import="ch.ethz.inf.dbproject.AddCaseServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h2>Add a new person of interest</h2>

<% 
if ((Boolean) session.getAttribute(AddCaseServlet.SESSION_USER_LOGGED_IN)) {
	// User is logged in. Display the details:
%>
	

 <h3>Please fill out details</h3>
	<form action="AddPersonOfInterest" method="post">
	<input type="hidden" name="action" value="addPersonOfInterest" />
	<table>
		<tr>
			<th>First name</th>
			<td><input type="text" name="first_name" value="" /></td>
		</tr>
		<tr>
			<th>Last name</th>
			<td><input type="text" name="last_name" value="" /></td>
		</tr>
		<tr>
			<th>Date of birth</th>
			<td><input type="date" name="date_of_birth" value="" /></td>
		</tr>
		<tr>
			<th colspan="5">
				<input type="submit" value="Add" />
			</th>
		</tr>
	</table>
	</form>

	


<%
//TODO: Display cases opened by the user

//TODO: Add possibility to create new case (requires a form) 
	
} else {
	// User not logged in. Display the login form.
%>
    <h3>Not permitted!</h3>


<%
}
%>

<%@ include file="Footer.jsp" %>