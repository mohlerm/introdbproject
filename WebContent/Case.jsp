<%@page import="ch.ethz.inf.dbproject.model.User"%>
<%@page import="ch.ethz.inf.dbproject.model.Status"%>
<%@page import="java.util.List"%>
<%@page import="ch.ethz.inf.dbproject.util.UserManagement"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>
<% final User user = (User) session.getAttribute(UserManagement.SESSION_USER); %>

<h1>Case Details</h1>

<hr/>

<%=session.getAttribute("caseTable")%>

<hr/>

<%
if (user != null) {
	// User is logged in. He can add a comment
%>
	<% List<Status> statusList = (List<Status>) session.getAttribute("statusList"); %>
	<% List<PersonOfInterest> personOfInterestList = (List<PersonOfInterest>) session.getAttribute("personOfInterestList"); %>
	<% List<PoiCategory> poiCategoryList = (List<PoiCategory>) session.getAttribute("poiCategoryList"); %>
	<form action="Case" method="get">
		<input type="hidden" name="id" value="<%= session.getAttribute("caseId") %>" />
		<input type="hidden" name="user_id" value="<%= user.getName() %>" />
		<br />
		Change Status
		<select name="status" id="statusId">
		<% for(Status s : statusList) { %>
		  		<option value="<%=s.getStatus()%>"><%= s.getStatus() %></option>
		<% } %>
        </select>
        <input type="hidden" name="action" value="change_status" />
		<input type="submit" value="apply" />

		</form>
		
		<% if(session.getAttribute("caseStatusString").equals("open")) {%>
			<a href="ModifyCase?id=<%=session.getAttribute("caseId")%>">
   			<button>Modify Case</button>
			</a>
		<% } %>	
	<form action="Case" method="get">	
		<input type="hidden" name="id" value="<%= session.getAttribute("caseId") %>" />
		<br />
		Category: 
		<select name="category" id="categoryId">
			<% Category aCat = (Category) session.getAttribute("aCat"); %>
			<% List<Category> categoryList = (List<Category>) session.getAttribute("categoryList"); %>
			<% for(Category c : categoryList) { %>
			<%   if(c.getParent() != null) { 
				   if(aCat.getName().equals(c.getName())) {%>
					<option value="<%=c.getName()%>" selected >&nbsp&nbsp<%=c.getName()%></option>
				<% } else { %>
		  			<option value="<%=c.getName()%>" >&nbsp&nbsp<%=c.getName()%></option>
		  		<% } %>
		  	  <% } else { %>
		  	<option value="<%=c.getName()%>" disabled><%=c.getName()%></option>
			<% } } %>
		</select>
	<% if(session.getAttribute("caseStatusString").equals("open")) {%>
	    <input type="hidden" name="action" value="modify_category" />
		<input type="submit" value="apply" /></form>
	<% } %>

	<form action="Case" method="get">
		<input type="hidden" name="id" value="<%= session.getAttribute("caseId") %>" />
		<% if(session.getAttribute("caseStatusString").equals("open")) {%>
		Add Person of Interest
		<select name="person" id="personsId">
		<% for(PersonOfInterest p : personOfInterestList) { %>
		  		<option value="<%=p.getId()%>"><%= p.getFirstName() + " " + p.getLastName() %></option>
		<% } %>
        </select>
        as
        <select name="poi_category" id="poi_categoryId">
		<% for(PoiCategory p : poiCategoryList) { %>
		<option value="<%=p.getName()%>"><%= p.getName() %></option>
		<% } %>
        </select>
		<input type="submit" value="add" />
		<% } %>
		<input type="hidden" name="action" value="add_person" />
		<br />
	</form>
	
	<form action="Case" method="get">
		<input type="hidden" name="id" value="<%= session.getAttribute("caseId") %>" />
		<% if(session.getAttribute("caseStatusString").equals("open")) {%>
		Add
		<select name="person" id="personsId">
		<% for(PersonOfInterest p : personOfInterestList) { %>
		  		<option value="<%=p.getId()%>"><%= p.getFirstName() + " " + p.getLastName() %></option>
		<% } %>
        </select>
        as Convict for
		<select name="convictCategory" id="convictCategoryId">
		<% for(Category c : categoryList) { %>
		<% if(c.getParent() != null) { %>
			<% if(request.getParameter("category") != null && request.getParameter("category").equals(c.getName())) { 
				%>
				<option value="<%=c.getName()%>" selected >&nbsp&nbsp<%=c.getName()%></option>
			<% } else {%>
	  			<option value="<%=c.getName()%>" >&nbsp&nbsp<%=c.getName()%></option>
	  			<% } %>
	  	<% } else { %>
	  	<option value="<%=c.getName()%>" disabled><%=c.getName()%></option>
		<% } } %></select>
       
        with start date
		<input type="date" name="startDate" value="" />
		and end date
		<input type="date" name="endDate" value="" />
		<input type="submit" value="add" />
		<% } %>
		<input type="hidden" name="action" value="add_conviction" />
		<br />
	</form>

	<form action="Case" method="post">
		<input type="hidden" name="id" value="<%= session.getAttribute("caseId") %>" />
		<input type="hidden" name="user_id" value="<%= user.getName() %>" />
		<% if(session.getAttribute("caseStatusString").equals("open")) {%>
		<br />
		Add Note
		<br />
		<input type="hidden" name="action" value="add_note" />
		<textarea rows="4" cols="50" name="note"></textarea>
		<br />

		<input type="submit" value="Submit Note" />
		<% }%>

	</form>
<hr/>
<%
}
%>

<%
	//TODO Display existing comments
	//session.getAttribute("commentTable")
%>
<h2>Suspects</h2>

<%=session.getAttribute("suspectsTable")%>

<hr/>

<h2>Witnesses</h2>

<%=session.getAttribute("witnessesTable")%>

<hr/>

<h2>Convictions</h2>

<%=session.getAttribute("convictionsTable")%>

<hr/>

<h2>Notes</h2>

<%=session.getAttribute("notesTable")%>

<hr/>

<%@ include file="Footer.jsp"%>