<%@page import="ch.ethz.inf.dbproject.model.*"%>
<%@page import="java.util.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	
	<head>
	    <link href="style.css" rel="stylesheet" type="text/css">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Law Enforcement Project</title>
	</head>

	<body>

		<!-- Header -->
		
		<table id="masterTable" cellpadding="0" cellspacing="0">
			<tr>
				<th id="masterHeader" colspan="2">
				<table id="newHeader">
				<th>
					<h1>Law Enforcement Project</h1>
					Project by Till Haug and Marcel Mohler 
				<% final User user2 = (User) session.getAttribute("LOGGED_IN_USER"); 
				if(user2 != null) {%>
				<br /><br />
					Currently logged in user: <%= user2.getName() %> - <a href="User?action=logout">(Logout)</a>
				<%}%>
				</th>
				<th>
				<%if(user2 != null) {%>
				<br /><br />
					<img src="data:image/jpg;base64,<%=session.getAttribute("userimage")%>" height="140" width="100" />
				<%}%>
				</th>
				</table>
				</th>
			</tr>
			

			
			<tr id="masterContent">
				<td id="masterContentMenu">
					<div class="menuDiv1"></div>
					<div class="menuDiv1"><a href="Home">Home</a></div>
					<div class="menuDiv1"><a href="Cases">All cases</a></div>
	       <%
				List<Status> status = DatastoreInterfaceSingleton.getInstance().getAllStatus();

				for(Status s : status) {
						%>
						  <div class="menuDiv2"><a href="Cases?filter=<%=s.getStatus()%>"><%=s.getStatus()%></a></div>
						<%
				}
			
			%>
					<div class="menuDiv2"><a href="Cases?filter=recent">Recent</a></div>
					<div class="menuDiv2"><a href="Cases?filter=oldest">Oldest Unsolved</a></div>
					<div class="menuDiv1"><a href="Categories">Categories</a></div>
			<%
				List<Category> categories = DatastoreInterfaceSingleton.getInstance().getAllCategories();
				List<Category> parent = new ArrayList<Category>();
				
				for(Category category : categories) {
					if(category.getParent() == null) {
						%>
						  <div class="menuDiv2"><a href="Cases?category=<%=category.getName()%>"><%=category.getName()%></a></div>
						<%
						for(Category subcategory : categories) {
							if(subcategory.getParent() != null && subcategory.getParent().equals(category.getName())) {
							  %>
								<div class="menuDiv3"><a href="Cases?category=<%=subcategory.getName()%>"><%=subcategory.getName()%></a></div>
							 <%
							}
						}
					}
				}
			
			%>
					<div class="menuDiv1"><a href="Convictions">Convictions</a></div>
					<div class="menuDiv1"><a href="PersonsOfInterest">Persons</a></div>
					<div class="menuDiv1"><a href="Search">Search</a></div>
					<div class="menuDiv1"><a href="User">User Profile</a></div>
					
				</td>
				
				<td id="masterContentPlaceholder">
				
