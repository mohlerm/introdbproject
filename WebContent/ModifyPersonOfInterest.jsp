<%@page import="ch.ethz.inf.dbproject.ModifyPersonOfInterestServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h2>Modify an existing person of interest</h2>

<% 
if ((Boolean) session.getAttribute(ModifyPersonOfInterestServlet.SESSION_USER_LOGGED_IN)) {
	// User is logged in. Display the details:
		PersonOfInterest aPoi = (PersonOfInterest) session.getAttribute("aPoi");
%>
	

 <h3>Please fill out details</h3>
	<form action="ModifyPersonOfInterest" method="post">
	<input type="hidden" name="action" value="modifyPersonOfInterest" />
	<table>
		<tr>
			<th>First name</th>
			<td><input type="text" name="first_name" value="<%=aPoi.getFirstName()%>" /></td>
		</tr>
		<tr>
			<th>Last name</th>
			<td><input type="text" name="last_name" value="<%=aPoi.getLastName()%>" /></td>
		</tr>
		<tr>
			<th>Date of birth</th>
			<td><input type="date" name="date_of_birth" value="<%=aPoi.getDateOfBirth()%>" /></td>
		</tr>
		<tr>
			<th colspan="5">
				<input type="submit" value="save" />
			</th>
		</tr>
	</table>
	</form>

	


<%
//TODO: Display cases opened by the user

//TODO: Add possibility to create new case (requires a form) 
	
} else {
	// User not logged in. Display the login form.
%>
    <h3>Not permitted!</h3>


<%
}
%>

<%@ include file="Footer.jsp" %>