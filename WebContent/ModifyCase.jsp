<%@page import="ch.ethz.inf.dbproject.ModifyCaseServlet"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h2>Modify an existing case</h2>

<% 
if ((Boolean) session.getAttribute(ModifyCaseServlet.SESSION_USER_LOGGED_IN)) {
	// User is logged in. Display the details:
	Case aCase = (Case) session.getAttribute("aCase");
	Category aCat = (Category) session.getAttribute("aCat");
%>
	

 <h3>Please fill out details</h3>
	<form action="ModifyCase" method="post">
	<input type="hidden" name="action" value="modifyCase" />
	<table>
		<tr>
			<th>Title</th>
			<td><input type="text" name="title" value="<%=aCase.getTitle() %>" /></td>
		</tr>
		<tr>
			<th>Date</th>
			<td><input type="date" name="date" value="<%=aCase.getDate() %>" /></td>
		</tr>
		<tr>
			<th>Time</th>
			<td><input type="time" name="time" value="<%=aCase.getTime() %>" /></td>
		</tr>
		<tr>
			<th>Location</th>
			<td><input type="text" name="location" value="<%=aCase.getLocation() %>" /></td>
		</tr>
		<tr>
			<th>Description</th>
			<td><input type="text" name="description" value="<%=aCase.getDescription() %>" /></td>
		</tr>
		<tr>
			<th>Category</th>
			<td><select name="category" id="categoryId">
			<% List<Category> categoryList = (List<Category>) session.getAttribute("categoryList"); %>
			<% for(Category c : categoryList) { %>
			<%   if(c.getParent() != null) { 
				   if(aCat.getName().equals(c.getName())) {%>
					<option value="<%=c.getName()%>" selected >&nbsp&nbsp<%=c.getName()%></option>
				<% } else { %>
		  			<option value="<%=c.getName()%>" >&nbsp&nbsp<%=c.getName()%></option>
		  		<% } %>
		  	  <% } else { %>
		  	<option value="<%=c.getName()%>" disabled><%=c.getName()%></option>
			<% } } %></select></td>
		</tr>
		<tr>
			<th colspan="5">
				<input type="submit" value="save" />
			</th>
		</tr>
	</table>
	</form>

	


<%
//TODO: Display cases opened by the user

//TODO: Add possibility to create new case (requires a form) 
	
} else {
	// User not logged in. Display the login form.
%>
    <h3>Not permitted!</h3>


<%
}
%>

<%@ include file="Footer.jsp" %>