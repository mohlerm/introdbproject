<%@page import="ch.ethz.inf.dbproject.UserServlet"%>
<%@page import="ch.ethz.inf.dbproject.model.*"%>
<%@page import="java.io.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="Header.jsp" %>

<h2>Your Account</h2>

<% 
if ((Boolean) session.getAttribute(UserServlet.SESSION_USER_LOGGED_IN)) {
	// User is logged in. Display the details:
%>
	

<img src="data:image/jpg;base64,<%=session.getAttribute("userimage")%>" height="140" width="100" />
<br />
Currently logged in user: <%= session.getAttribute(UserServlet.SESSION_USER_DETAILS) %>


<table>
	<tr>
	<th>
		
<form action="AddCase">
    <input type="submit" value="Create new case">
</form>
</th>
<th>
<form action="User" method="get">
<input type="hidden" name="action" value="logout" />
<input type="submit" value="Logout" />
</form>	
</th>
</tr>
</table>

<h2>Opened cases by you</h2>
<%=session.getAttribute("userCases")%>

<%

//TODO: Add possibility to create new case (requires a form) 
	
} else {
	// User not logged in. Display the login form.
%>
    <h3>Login</h3>
	<form action="User" method="post">
	<input type="hidden" name="action" value="login" />
	<table>
		<tr>
			<th>Username</th>
			<td><input type="text" name="username" value="" /></td>
		</tr>
		<tr>
			<th>Password</th>
			<td><input type="password" name="password" value="" /></td>
		</tr>
		<tr>
			<th colspan="2">
				<input type="submit" value="Login" />
			</th>
		</tr>
	</table>
	<h3>Registration</h3>
	</form>
		<form action="User" method="post" enctype="multipart/form-data">
	<input type="hidden" name="action" value="registration" />
	<table>
		<tr align = "left">
			<th>Username</th>
			<td><input type="text" name="username" /></td>
		</tr>
		<tr align = "left">
			<th>Password</th>
			<td><input type="password" name="password" /></td>
		</tr>
		<tr align = "left">
			<th>Confirm Password</th>
			<td><input type="password" name="password2"  /></td>
		</tr>
		<tr align = "left">
			<th>Registration key</th>
			<td><input type="password" name="key"  /></td>
		</tr>
		<tr align = "left">
  		<th>Upload your profile picture:</th>
    	<td><input name="profilepicture" type="file" /></td>
		</tr>
		<tr>
			<th colspan="2">
				<input type="submit" value="Registration" />
			</th>
		</tr>
	</table>
	</form>

<%
}
%>

<%@ include file="Footer.jsp" %>