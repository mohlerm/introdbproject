package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.*;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.util.UserManagement;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class of Case Details Page
 */
@WebServlet(description = "Displays a specific case.", urlPatterns = { "/Case" })
public final class CaseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = DatastoreInterfaceSingleton.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CaseServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

		final HttpSession session = request.getSession(true);
		final User loggedUser = UserManagement.getCurrentlyLoggedInUser(session);

		final String idString = request.getParameter("id");
		final Integer id = Integer.parseInt(idString);

		final String action = request.getParameter("action");
		if (action != null && action.trim().equals("change_status") && loggedUser != null) {
			try {	
				
				dbInterface.changeCaseStatus(id, request.getParameter("status"));
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//this.getServletContext().getRequestDispatcher("/Case.jsp").forward(request, response); return;
			}
		}
		
		if (action != null && action.trim().equals("modify_case") && loggedUser != null) {
			try {	
				
				this.getServletContext().getRequestDispatcher("/ModifyCase.jsp").forward(request, response); return;
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//this.getServletContext().getRequestDispatcher("/Case.jsp").forward(request, response); return;
			}
		}
		
		if (action != null && action.trim().equals("add_person") && loggedUser != null) {
			try {
				
				dbInterface.linkPersonToCase(Integer.parseInt(request.getParameter("person")), id, request.getParameter("poi_category"));
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//this.getServletContext().getRequestDispatcher("/Case.jsp").forward(request, response); return;
			}
		}
		
		if (action != null && action.trim().equals("modify_category") && loggedUser != null) {
			try {
				
				dbInterface.modifyCaseCategory(request.getParameter("category"), id);
			} catch (NumberFormatException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//this.getServletContext().getRequestDispatcher("/Case.jsp").forward(request, response); return;
			}
		}
		
		if (action != null && action.trim().equals("add_conviction") && loggedUser != null) {
			try {
				
				dbInterface.linkConvictionToCase(Integer.parseInt(request.getParameter("person")), id, request.getParameter("startDate"), request.getParameter("endDate"), request.getParameter("convictCategory"));
			} catch (NumberFormatException | SQLException | ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//this.getServletContext().getRequestDispatcher("/Case.jsp").forward(request, response); return;
			}
		}
		
		if (action != null && action.trim().equals("deleteNote")) {
			try {
				this.dbInterface.deleteNoteById(Integer.parseInt(request.getParameter("noteId")));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if (action != null && action.trim().equals("unlinkPerson")) {
			try {
				this.dbInterface.unlinkPersonFromCase(Integer.parseInt(request.getParameter("pid")),Integer.parseInt(request.getParameter("id")));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		

		if (idString == null) {
			this.getServletContext().getRequestDispatcher("/Cases").forward(request, response);
		}

		try {
			final Case aCase = this.dbInterface.getCaseById(id);
			final List<Status> statusList = this.dbInterface.getAllStatus();
			final List<PersonOfInterest> personOfInterestList = this.dbInterface.getAllPersonsOfInterest();
			final List<PoiCategory> poiCategoryList = this.dbInterface.getAllPoiCategories();
			List<Category> categories = this.dbInterface.getAllCategories();
			Category aCat = this.dbInterface.getCategoryByCaseId(id);
			List<Category> categoryList = new ArrayList<Category>();
			for(Category category : categories) {
				if(category.getParent() == null) {
					categoryList.add(category);
					for(Category subcategory : categories) {
						if(subcategory.getParent() != null && subcategory.getParent().equals(category.getName())) {
							categoryList.add(subcategory);
						}
					}
				}
			}
			session.setAttribute("aCat", aCat);
			session.setAttribute("categoryList", categoryList);
			session.setAttribute("statusList", statusList);
			session.setAttribute("caseId", id);
			session.setAttribute("personOfInterestList", personOfInterestList);
			session.setAttribute("poiCategoryList", poiCategoryList);
			session.setAttribute("caseStatusString", aCase.getStatus().toString());	
			
			/*******************************************************
			 * RECREATE TABLES
			 *******************************************************/
			/*******************************************************
			 * Construct a table to present all properties of a case
			 *******************************************************/
			final BeanTableHelper<Case> table = new BeanTableHelper<Case>(
					"cases" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					Case.class 	/* The class of the objects (rows) that will be displayed */
			);
	
			// Add columns to the new table
	
			/*
			 * Column 1: The name of the item (This will probably have to be changed)
			 */
			
			table.addBeanColumn("Title", "title");
	
			/*
			 * Columns 2 & 3: Some random fields. These should be replaced by i.e. funding progress, or time remaining
			 */	
			table.addBeanColumn("CaseID", "id");
			table.addBeanColumn("Date", "date");
			table.addBeanColumn("Time", "time");
			table.addBeanColumn("Location", "location");
			table.addBeanColumn("opened by", "user");
			table.addBeanColumn("Description", "description");
			table.addBeanColumn("Status", "status");
			table.addObject(aCase);
			table.setVertical(true);		
			
			session.setAttribute("caseTable", table);
			
			/*******************************************************
			 * Construct a table to present all Suspects of a case
			 *******************************************************/
			final BeanTableHelper<PersonOfInterest> suspectsTable = new BeanTableHelper<PersonOfInterest>(
					"cases" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					PersonOfInterest.class 	/* The class of the objects (rows) that will be displayed */
			);
			suspectsTable.addBeanColumn("Firstname", "firstName");
			suspectsTable.addBeanColumn("Lastname", "lastName");
			suspectsTable.addBeanColumn("Date of birth", "dateOfBirth");
			suspectsTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"View Person" 	/* What should be displayed in every row */,
					"PersonOfInterest?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
			if(loggedUser != null) {
			suspectsTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"Unlink" 	/* What should be displayed in every row */,
					"Case?id=" + request.getParameter("id") +"&action=unlinkPerson&pid=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
			}
			suspectsTable.setVertical(false);			
	
			/*******************************************************
			 * Construct a table to present all Witnesses of a case
			 *******************************************************/
			final BeanTableHelper<PersonOfInterest> witnessesTable = new BeanTableHelper<PersonOfInterest>(
					"cases" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					PersonOfInterest.class 	/* The class of the objects (rows) that will be displayed */
			);	
			witnessesTable.addBeanColumn("Firstname", "firstName");
			witnessesTable.addBeanColumn("Lastname", "lastName");
			witnessesTable.addBeanColumn("Date of birth", "dateOfBirth");
			witnessesTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"View Person" 	/* What should be displayed in every row */,
					"PersonOfInterest?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
			if(loggedUser != null) {
			witnessesTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"Unlink" 	/* What should be displayed in every row */,
					"Case?id=" + request.getParameter("id") +"&action=unlinkPerson&pid=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
			}
			witnessesTable.setVertical(false);					
	
			for(PersonOfInterest p : this.dbInterface.getAllPersonsOfInterestByCaseId(id)) {
				String cat = p.getCategoryById(id);
				if( cat != null) {
					if(cat.equals("suspect")) {
						suspectsTable.addObject(p);
					} else if(cat.equals("witness")) {
						witnessesTable.addObject(p);
					}
				}
			}
			
			session.setAttribute("suspectsTable", suspectsTable);
			session.setAttribute("witnessesTable", witnessesTable);
			
			
			/*******************************************************
			 * Construct a table to present all Convictions of a case
			 *******************************************************/
			final BeanTableHelper<Conviction> convictionsTable = new BeanTableHelper<Conviction>(
					"cases" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					Conviction.class 	/* The class of the objects (rows) that will be displayed */
			);
			convictionsTable.addBeanColumn("Firstname", "firstName");
			convictionsTable.addBeanColumn("Lastname", "lastName");
			convictionsTable.addBeanColumn("Date of birth", "dateOfBirth");
			convictionsTable.addBeanColumn("Start date", "startDate");
			convictionsTable.addBeanColumn("End date", "endDate");
			convictionsTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"View Person" 	/* What should be displayed in every row */,
					"PersonOfInterest?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"personId" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
	
			convictionsTable.setVertical(false);	

			convictionsTable.addObjects(this.dbInterface.getConvictionsByCaseId(id));
			session.setAttribute("convictionsTable", convictionsTable);
			
			/*******************************************************
			 * Construct a table to present all notes of a case
			 *******************************************************/
			final BeanTableHelper<Note> notesTable = new BeanTableHelper<Note>(
					"cases" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					Note.class 	/* The class of the objects (rows) that will be displayed */
			);
			notesTable.addBeanColumn("Note", "partialText");
			notesTable.addBeanColumn("Username", "userName");
			notesTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"View full note" 	/* What should be displayed in every row */,
					"Note?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
			if(loggedUser != null) {
			notesTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"delete" 	/* What should be displayed in every row */,
					"Case?id=" + request.getParameter("id") +"&action=deleteNote&noteId=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
			}
			notesTable.setVertical(false);	
			notesTable.addObjects(this.dbInterface.getNotesByCase(id));
			
			session.setAttribute("notesTable", notesTable);
			
		} catch (final Exception ex) {
			ex.printStackTrace();
			this.getServletContext().getRequestDispatcher("/Cases.jsp").forward(request, response); return;
		}
		
			
		this.getServletContext().getRequestDispatcher("/Case.jsp").forward(request, response);
	}
	
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

		final HttpSession session = request.getSession(true);
		final User loggedUser = UserManagement.getCurrentlyLoggedInUser(session);

		final String action = request.getParameter("action");
		if (action != null && action.trim().equals("add_note") 	&& loggedUser != null) {
			// TODO: 
			try {
				dbInterface.insertNote(request.getParameter("note"), loggedUser.getName(), request.getParameter("id"), null);
				
				
				/*******************************************************
				 * Construct a table to present all notes of a case
				 *******************************************************/
				final BeanTableHelper<Note> notesTable = new BeanTableHelper<Note>(
						"cases" 		/* The table html id property */,
						"casesTable" /* The table html class property */,
						Note.class 	/* The class of the objects (rows) that will be displayed */
				);
				notesTable.addBeanColumn("Note", "partialText");
				notesTable.addBeanColumn("Username", "userName");
				notesTable.addLinkColumn(""	/* The header. We will leave it empty */,
						"View full note" 	/* What should be displayed in every row */,
						"Note?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
						"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);

				notesTable.setVertical(false);	
				notesTable.addObjects(this.dbInterface.getNotesByCase(Integer.parseInt(request.getParameter("id"))));
				
				session.setAttribute("notesTable", notesTable);
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				this.getServletContext().getRequestDispatcher("/Case.jsp").forward(request, response); return;
			}
		}
		

		

		this.getServletContext().getRequestDispatcher("/Case.jsp").forward(request, response);
	}
}