package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.Conviction;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.DatastoreInterfaceSingleton;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class of Convictions list page
 */
@WebServlet(description = "The home page of the project", urlPatterns = { "/Convictions" })
public final class ConvictionsServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = DatastoreInterfaceSingleton.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ConvictionsServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) 
			throws ServletException, IOException {

		final HttpSession session = request.getSession(true);

		/*******************************************************
		 * Construct a table to present all our results
		 *******************************************************/
		final BeanTableHelper<Conviction> table = new BeanTableHelper<Conviction>(
				"cases" 		/* The table html id property */,
				"casesTable" /* The table html class property */,
				Conviction.class 	/* The class of the objects (rows) that will bedisplayed */
		);

		// Add columns to the new table

		/*
		 * Column 1: The name of the item (This will probably have to be changed)
		 */
		
		table.addBeanColumn("First Name", "firstName");

		/*
		 * Columns 2 & 3: Some random fields. These should be replaced by i.e. funding progress, or time remaining
		 */	
		table.addBeanColumn("Last Name", "lastName");
		table.addBeanColumn("Case title", "title");
		table.addBeanColumn("Start date", "startDate");
		table.addBeanColumn("End date", "endDate");
		table.addBeanColumn("Category", "category");
		/*
		 * Column 4: This is a special column. It adds a link to view the
		 * Project. We need to pass the POI identifier to the url.
		 */
		table.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Person" 	/* What should be displayed in every row */,
				"PersonOfInterest?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"personId" 			/* For every POI displayed, the ID will be retrieved and will be attached to the url base above */);
		table.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Case" 	/* What should be displayed in every row */,
				"Case?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"caseId" 			/* For every POI displayed, the ID will be retrieved and will be attached to the url base above */);

		// Pass the table to the session. This will allow the respective jsp page to display the table.
		

		try {
			table.addObjects(this.dbInterface.getAllConvictions());
		} catch (SQLException e) {
			// TODO implement proper error display
			e.printStackTrace();
		}
		session.setAttribute("convictionsTable", table);
		
		this.getServletContext().getRequestDispatcher("/Convictions.jsp").forward(request, response);
	}
}