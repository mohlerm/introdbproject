package ch.ethz.inf.dbproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import ch.ethz.inf.dbproject.model.*;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.util.UserManagement;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;


/**
 * Servlet implementation class of Categories Page
 */
@WebServlet(description = "Page that allows to add a case.", urlPatterns = { "/Categories" })
public final class CategoriesServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = DatastoreInterfaceSingleton.getInstance();

	public final static String SESSION_USER_LOGGED_IN = "userLoggedIn";
	public final static String SESSION_USER_DETAILS = "userDetails";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CategoriesServlet() {
		super();
	}
	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {
		final HttpSession session = request.getSession(true);
		final User loggedUser = UserManagement.getCurrentlyLoggedInUser(session);
		

		
		
		if (loggedUser == null) {
			// Not logged in!
			session.setAttribute(SESSION_USER_LOGGED_IN, false);
		} else {
			session.setAttribute(SESSION_USER_LOGGED_IN, true);
			// Logged in
			final String action = request.getParameter("action");
			if (action != null && action.trim().equals("addCategory")) {
				final String topCategory = request.getParameter("topCategory");
				final String subCategory = request.getParameter("subCategory");
				try {
					this.dbInterface.insertCategory(topCategory, subCategory);
				} catch ( SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
		if (loggedUser == null) {
			// Not logged in!
			session.setAttribute(SESSION_USER_LOGGED_IN, false);
		} else {
			session.setAttribute(SESSION_USER_LOGGED_IN, true);
			// Logged in
			final String action = request.getParameter("action");
			if (action != null && action.trim().equals("deleteCategory")) {
				final String name = request.getParameter("name");
				try {
					this.dbInterface.deleteSubCategory(name);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		}
		
		try {
			List<Category> categories = this.dbInterface.getAllCategories();
			
			List<Category> categoryList = new ArrayList<Category>();
			for(Category category : categories) {
				if(category.getParent() == null) {
					//category.setCount(this.dbInterface.getNumberOfCasesByCategory(category.getName()));
					//categoryList.add(category);
					for(Category subcategory : categories) {
						if(subcategory.getParent() != null && subcategory.getParent().equals(category.getName())) {
							subcategory.setCount(this.dbInterface.getNumberOfCasesByCategory(subcategory.getName()));
							categoryList.add(subcategory);
						}
					}
				}
			}

			/*******************************************************
			 * Construct a table to present all notes of a case
			 *******************************************************/
			final BeanTableHelper<Category> categoriesTable = new BeanTableHelper<Category>(
					"cases" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					Category.class 	/* The class of the objects (rows) that will be displayed */
			);
			categoriesTable.addBeanColumn("Top Category", "parent");
			categoriesTable.addBeanColumn("Sub Category", "name");
			categoriesTable.addBeanColumn("Amount", "count");
			if(loggedUser != null) {
			categoriesTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"delete" 	/* What should be displayed in every row */,
					"Categories?action=deleteCategory&name=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"name" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
			}
			categoriesTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"view cases" 	/* What should be displayed in every row */,
					"Cases?category=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"name" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
			
			
			categoriesTable.setVertical(false);	
			categoriesTable.addObjects(categoryList);
			
			session.setAttribute("categoriesTable", categoriesTable);
			
			session.setAttribute("categoryList", categories);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		this.getServletContext().getRequestDispatcher("/Categories.jsp").forward(request, response);
		
		
	}
	
}
