package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.*;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class Search
 */
@WebServlet(description = "The search page for cases", urlPatterns = { "/Search" })
public final class SearchServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	DatastoreInterface dbInterface;
		
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchServlet() {
        super();
        dbInterface = DatastoreInterfaceSingleton.getInstance();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		final HttpSession session = request.getSession(true);
		
		final BeanTableHelper<Conviction> ctable = new BeanTableHelper<Conviction>(
				"cases" 		/* The table html id property */,
				"casesTable" /* The table html class property */,
				Conviction.class 	/* The class of the objects (rows) that will bedisplayed */
		);


		ctable.addBeanColumn("First Name", "firstName");
		ctable.addBeanColumn("Last Name", "lastName");
		ctable.addBeanColumn("Case title", "title");
		ctable.addBeanColumn("Start date", "startDate");
		ctable.addBeanColumn("End date", "endDate");
		ctable.addBeanColumn("Category", "category");
		ctable.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Person" 	/* What should be displayed in every row */,
				"PersonOfInterest?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"personId" 			/* For every POI displayed, the ID will be retrieved and will be attached to the url base above */);
		ctable.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Case" 	/* What should be displayed in every row */,
				"Case?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"caseId" 			/* For every POI displayed, the ID will be retrieved and will be attached to the url base above */);

		
		final BeanTableHelper<PersonOfInterest> ptable = new BeanTableHelper<PersonOfInterest>(
				"cases" 		/* The table html id property */,
				"casesTable" /* The table html class property */,
				PersonOfInterest.class 	/* The class of the objects (rows) that will bedisplayed */
		);


		ptable.addBeanColumn("First Name", "firstName");
		ptable.addBeanColumn("Last Name", "lastName");
		ptable.addBeanColumn("Date of birth", "dateOfBirth");
		ptable.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Person" 	/* What should be displayed in every row */,
				"PersonOfInterest?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"id" 			/* For every POI displayed, the ID will be retrieved and will be attached to the url base above */);

		
		final BeanTableHelper<Case> table = new BeanTableHelper<Case>(
				"cases" 		/* The table html id property */,
				"casesTable" /* The table html class property */,
				Case.class 	/* The class of the objects (rows) that will bedisplayed */
		);


		table.addBeanColumn("Title", "title");
		table.addBeanColumn("Date", "date");
		table.addBeanColumn("Time", "time");
		table.addBeanColumn("Status", "status");
		table.addBeanColumn("Opened by", "user");
		table.addLinkColumn(""	/* The header. We will leave it empty */,
				"View Case" 	/* What should be displayed in every row */,
				"Case?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
				"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
		

		// The filter parameter defines what to show on the cases page
		final String filter = request.getParameter("filter");

		if (filter != null) {
		
			if(filter.equals("description")) {
				final String name = request.getParameter("description");
				final String status = request.getParameter("status");
				try {
					session.setAttribute("result", table);
					table.addObjects(this.dbInterface.searchByName(name,(status.compareTo("Any status")==0) ? "" : status));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			else if(filter.equals("poi")) {
				final String name = request.getParameter("person");
				try {
					session.setAttribute("result", ptable);
					ptable.addObjects(this.dbInterface.searchPoi(name));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else if(filter.equals("conviction")) {
				final String name = request.getParameter("name");
				final String category = request.getParameter("type");
				final String date = request.getParameter("convictedBy");
				try {
					session.setAttribute("result", ctable);
					ctable.addObjects(this.dbInterface.searchConviction(name, (category.compareTo("Any type") == 0) ? "" : category, date));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		// Finally, proceed to the Seaech.jsp page which will render the search results
        this.getServletContext().getRequestDispatcher("/Search.jsp").forward(request, response);	        
	}
}