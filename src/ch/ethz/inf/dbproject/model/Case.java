package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public final class Case {
	private final int id;
	private final String title;
	private final String description;
	private final String location;
	private final Date date;
	private final Time time;
	private final User user;
	private final Status status;
	private final List<Note> notes;
	private final DatastoreInterface dataStoreInterface;
	
	public Case(final ResultSet rs) throws SQLException {
		dataStoreInterface = DatastoreInterfaceSingleton.getInstance();
		this.id = rs.getInt("id");
		this.description = rs.getString("description");
		this.title = rs.getString("title");
		this.location = rs.getString("location");
		this.date = rs.getDate("date");
		this.time = rs.getTime("time");
		this.user = dataStoreInterface.getUser(rs.getString("user_name"));
		this.notes = dataStoreInterface.getNotesByCase(id);
		this.status = new Status(rs.getString("status"));
	}
    public Case(Tuple tuple) throws SQLException {
        dataStoreInterface = DatastoreInterfaceSingleton.getInstance();
        this.id = tuple.getInt(0);
        this.title = tuple.get(1);
        this.date = tuple.getDate(2);
        this.time = tuple.getTime(3);
        this.description = tuple.get(4);
        this.location = tuple.get(5);
        this.user = dataStoreInterface.getUser(tuple.get(6));
        this.status = new Status(tuple.get(7));
        this.notes = dataStoreInterface.getNotesByCase(id);
    }

	public User getUser() {
		return user;
	}


	public List<Note> getNotes() {
		return notes;
	}


	public int getId() {
		return id;
	}


	public String getTitle() {
		return title;
	}


	public String getDescription() {
		return description;
	}


	public String getLocation() {
		return location;
	}


	public Date getDate() {
		return date;
	}


	public Time getTime() {
		return time;
	}


	public Status getStatus() {
		return status;
	}


}