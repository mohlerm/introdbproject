package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/**
 * Object that represents a registered in user.
 */
public final class PersonOfInterest {

	private final int id;
	private final String firstName;
	private final String lastName;
	private final Date dateOfBirth;
	private HashMap<Integer, String> cases;
	
	public PersonOfInterest(final ResultSet rs) throws SQLException {
		this.id = rs.getInt("id");
		this.firstName = rs.getString("first_name");
		this.lastName = rs.getString("last_name");
		this.dateOfBirth = rs.getDate("date_of_birth");
		cases = new HashMap<Integer, String>();
		try {
			this.cases.put(rs.getInt("case_id"), rs.getString("poi_category_name"));
		} catch (SQLException e) {
			cases = null;
		}
	}
    public PersonOfInterest(Tuple tuple)  {
        this.id = tuple.getInt(0);
        this.firstName = tuple.get(1);
        this.lastName = tuple.get(2);
        this.dateOfBirth = tuple.getDate(3);
        cases = new HashMap<Integer, String>();
        try {
            this.cases.put(tuple.getInt(5), tuple.get(6));
        } catch (Exception e) {
            cases = null;
        }
    }

	public int getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	
	public String getCategoryById(int c) {
		if(cases != null) {
			if(cases.get(c) != null) {
				return cases.get(c);
			}
		}
		return "notincluded";
	}
	


}
