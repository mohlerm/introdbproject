package ch.ethz.inf.dbproject.model;

import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;


/**
 * This class should be the interface between the web application
 * and the database. Keeping all the data-access methods here
 * will be very helpful for part 2 of the project.
 */
public interface DatastoreInterface {
    /**
     *
     * @param username name of the user
     * @return true if user exists
     * @throws SQLException
     */
    boolean userExists(String username) throws SQLException;
    /**
     *
     * @param username name of the user
     * @param password will be hashed and salted
     * @param picture picture as byte array
     * @throws NoSuchAlgorithmException
     * @throws SQLException
     */
    void insertUser(String username, String password, byte[] picture) throws SQLException, NoSuchAlgorithmException;
    /**
     *
     * @param parent parent of the inserting category
     * @param name
     * @throws SQLException
     */
    void insertCategory(String parent, String name) throws SQLException;
    /**
     *
     * @param text
     * @param userName
     * @param caseId
     * @param personOfInterestId
     * @throws SQLException
     */
    void insertNote(String text, String userName, String caseId, String personOfInterestId) throws SQLException;
    /**
     *
     * @param title
     * @param date
     * @param time
     * @param description
     * @param location
     * @param user_name
     * @return the ID of the newly created case
     * @throws SQLException
     * @throws ParseException
     */
    int insertCase(String title, String date, String time, String description, String location, String user_name) throws SQLException, ParseException;
    /**
     *
     * @param firstName
     * @param lastName
     * @param birthDate
     * @throws SQLException
     * @throws ParseException
     */
    void insertPersonOfInterest(String firstName, String lastName, String birthDate) throws SQLException, ParseException;
    /**
     *
     * @param category
     * @param id
     * @throws SQLException
     */
    void linkCategoryToId(String category, int id) throws SQLException;
    /**
     *
     * @param idP person of interest ID
     * @param idC category ID
     * @param category string of the category of this link
     * @throws SQLException
     */
    void linkPersonToCase(int idP, int idC, String category) throws SQLException;
    /**
     *
     * @param idP person of interest ID
     * @param idC category ID
     * @param startDate
     * @param endDate
     * @param category
     * @throws SQLException
     * @throws ParseException
     */
    void linkConvictionToCase(int idP, int idC, String startDate, String endDate, String category) throws SQLException, ParseException;
    /**
     *
     * @param idP person of interest ID
     * @param idC category ID
     * @throws SQLException
     */
    void unlinkPersonFromCase(int idP, int idC) throws SQLException;
    /**
     *
     * @param username
     * @return
     * @throws SQLException
     */
    User getUser(String username) throws SQLException;
    /**
     *
     * @param username
     * @param password
     * @return true if user/pass is a valid login
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     */
    boolean isValidLogin(String username, String password) throws SQLException, NoSuchAlgorithmException;
    /**
     *
     * @param category
     * @return
     * @throws SQLException
     */
    List<Case> getCasesByCategory(String category) throws SQLException;
    /**
     *
     * @param status
     * @return a list of the cases with the matching status
     * @throws SQLException
     */
    List<Case> getCasesByStatus(String status) throws SQLException;
    /**
     *
     * @param username
     * @return a list of cases with the given username
     * @throws SQLException
     */
    List<Case> getCasesByUser(String username) throws SQLException;
    /**
     *
     * @param id
     * @return a list of cases with the given ID
     * @throws SQLException
     */
    Case getCaseById(Integer id) throws SQLException;
    /**
     *
     * @return all cases of the database
     * @throws SQLException
     */
    List<Case> getAllCases() throws SQLException;
    /**
     *
     * @return all categories of the database
     * @throws SQLException
     */
    List<Category> getAllCategories() throws SQLException;
    /**
     *
     * @return all convictions
     * @throws SQLException
     */
    List<Conviction> getAllConvictions() throws SQLException;
    /**
     *
     * @param parent
     * @return list of all subcategories of the given parent
     * @throws SQLException
     */
    List<Category> getSubCategories(String parent) throws SQLException;
    /**
     *
     * @param id
     * @return category with the given case id
     * @throws SQLException
     */
    Category getCategoryByCaseId(int id) throws SQLException;
    /**
     *
     * @param id
     * @return list of convictions
     * @throws SQLException
     */
    List<Conviction> getConvictionsByCaseId(int id) throws SQLException;
    /**
     *
     * @param id
     * @return list of convictions by person ID
     * @throws SQLException
     */
    List<Conviction> getConvictionsByPersonId(int id) throws SQLException;
    /**
     *
     * @return a list of all person of interest
     * @throws SQLException
     */
    List<PersonOfInterest> getAllPersonsOfInterest() throws SQLException;
    /**
     *
     * @param id
     * @return all person of interest of a given case
     * @throws SQLException
     */
    List<PersonOfInterest> getAllPersonsOfInterestByCaseId(int id) throws SQLException;
    /**
     *
     * @param id person of interest ID
     * @return all cases of a given person of interest ID
     * @throws SQLException
     */
    List<PoiCase> getAllCasesByPersonOfInterest(int id) throws SQLException;
    /**
     *
     * @param cat
     * @return a list of all PoiCase
     * @throws SQLException
     */
    List<PoiCase> getAllPersonsOfInterestByCategory(String cat) throws SQLException;
    /**
     *
     * @return a list of all possible Stati
     * @throws SQLException
     */
    List<Status> getAllStatus() throws SQLException;
    /**
     *
     * @return a list of all possible PoiCategories
     * @throws SQLException
     */
    List<PoiCategory> getAllPoiCategories() throws SQLException;
    /**
     *
     * @param category
     * @return the amount of cases of a given category
     * @throws SQLException
     */
    int getNumberOfCasesByCategory(String category) throws SQLException;
    /**
     *
     * @param id
     * @return the note with a given ID
     * @throws SQLException
     */
    Note getNoteById(int id) throws SQLException;
    /**
     *
     * @param caseId
     * @return a list of all notes of a case since a case can have multiple notes
     * @throws SQLException
     */
    List<Note> getNotesByCase(int caseId) throws SQLException;
//	public List<Note> getNotesByUser(String userName) throws SQLException;
    /**
     *
     * @param personOfInterestId
     * @return a list of all notes from a given person of interest ID
     * @throws SQLException
     */
    List<Note> getNotesByPersonOfInterest(int personOfInterestId) throws SQLException;
    /**
     *
     * @return a list of all parent categories
     * @throws SQLException
     */
    List<Category> getParentCategories() throws SQLException;
    /**
     *
     * @param id
     * @return
     * @throws SQLException
     */
    PersonOfInterest getPersonOfInterestById(Integer id) throws SQLException;
    /**
     *
     * @param id
     * @param status
     * @throws SQLException
     */
    void changeCaseStatus(Integer id, String status) throws SQLException;
    /**
     *
     * @param name
     * @param id
     * @throws SQLException
     */
    void modifyCaseCategory(String name, int id) throws SQLException;
    /**
     *
     * @param id
     * @param title
     * @param date
     * @param time
     * @param description
     * @param location
     * @param category
     * @throws SQLException
     * @throws ParseException
     */
    void modifyCase(int id, String title, String date, String time, String description, String location, String category) throws SQLException, ParseException;
    /**
     *
     * @param id
     * @param firstName
     * @param lastName
     * @param dateOfBirth
     * @throws SQLException
     * @throws ParseException
     */
    void modifyPersonOfInterest(int id, String firstName, String lastName, String dateOfBirth) throws SQLException, ParseException;
    /**
     *
     * @param id
     * @param status
     * @throws SQLException
     */
    void deleteCaseById(Integer id, Status status) throws SQLException;
    /**
     *
     * @param name
     * @throws SQLException
     */
    void deleteSubCategory(String name) throws SQLException;
    /**
     *
     * @param id
     * @throws SQLException
     */
    void deleteNoteById(int id) throws SQLException;
    /**
     *
     * @param name
     * @param status
     * @return
     * @throws SQLException
     */
    List<Case> searchByName(String name, String status) throws SQLException;
    /**
     *
     * @return
     * @throws SQLException
     */
    List<Case> getMostRecentCases() throws SQLException;
    /**
     *
     * @return
     * @throws SQLException
     */
    List<Case> getOldestUnsolvedCases() throws SQLException;
    /**
     *
     * @param name
     * @return
     * @throws SQLException
     */
    List<PersonOfInterest> searchPoi(String name) throws SQLException;
    /**
     *
     * @param name
     * @param category
     * @param date
     * @return
     * @throws SQLException
     */
    List<Conviction> searchConviction(String name, String category, String date) throws SQLException;
}