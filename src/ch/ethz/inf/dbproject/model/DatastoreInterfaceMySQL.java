package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.database.MySQLConnection;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This class should be the interface between the web application
 * and the database. Keeping all the data-access methods here
 * will be very helpful for part 2 of the project.
 */
public final class DatastoreInterfaceMySQL implements DatastoreInterface {

    @SuppressWarnings("unused")
    private Connection sqlConnection;
    //private static DatastoreInterfaceMySQL instance = null;

    public DatastoreInterfaceMySQL() {
        this.sqlConnection = MySQLConnection.getInstance().getConnection();
    }

//    public static DatastoreInterfaceMySQL getInstance() {
//        if(instance == null) {
//            instance = new DatastoreInterfaceMySQL();
//        }
//        return instance;
//    }

    public boolean userExists(String username) throws SQLException {
        PreparedStatement pstmt = sqlConnection.prepareStatement("SELECT name FROM user WHERE name = ?");
        pstmt.setString(1, username);
        pstmt.execute();
        return pstmt.getResultSet().next();
    }

    public void insertUser(String username, String password, byte[] picture) throws NoSuchAlgorithmException, SQLException {
        PreparedStatement pstmt;
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(("crime" + username + password).getBytes());
        pstmt = sqlConnection.prepareStatement("INSERT INTO user (`name`, `password`, `image`) VALUES (?,?,?)");
        pstmt.setString(1, username);
        pstmt.setBytes(2, messageDigest.digest());
        pstmt.setBytes(3, picture);
        pstmt.execute();
    }

    public void insertCategory(String parent, String name) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("INSERT INTO category (`parent`, `name`) VALUES (?,?)");
        if(parent == null) {
            pstmt.setNull(1, Types.VARCHAR);
        }
        else {
            pstmt.setString(1, parent);
        }
        pstmt.setString(2, name);
        pstmt.execute();
    }

    public void insertNote(String text, String userName, String caseId, String personOfInterestId) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("INSERT INTO note (`text`, `case_id`, `person_of_interest_id`, `user_name`) VALUES (?,?,?,?)");
        pstmt.setString(1, text);
        if(caseId == null) {
            pstmt.setNull(2, Types.INTEGER);
        }
        else {
            pstmt.setInt(2, Integer.parseInt(caseId));
        }

        if(personOfInterestId == null) {
            pstmt.setNull(3, Types.INTEGER);
        }
        else {
            pstmt.setInt(3, Integer.parseInt(personOfInterestId));
        }
        pstmt.setString(4, userName);
        pstmt.execute();
    }

    public int insertCase(String title, String date, String time, String description, String location, String user_name) throws SQLException, ParseException {
        PreparedStatement pstmt;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = null;
        Time convertedTime = null;
        if(!date.isEmpty()) {
            convertedDate = new Date(sdf.parse(date).getTime());
        }
        if(!time.isEmpty()) {
            sdf = new SimpleDateFormat("HH:mm");
            convertedTime = new Time(sdf.parse(time).getTime());
        }

        pstmt = sqlConnection.prepareStatement("INSERT INTO `case` (`title`,`date`,`time`,`description`,`location`,`user_name`,`status`) VALUES (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
        pstmt.setString(1, title);
        pstmt.setDate(2, convertedDate);
        pstmt.setTime(3, convertedTime);
        pstmt.setString(4, description);
        pstmt.setString(5, location);
        pstmt.setString(6, user_name);
        pstmt.setString(7, "open");
        pstmt.execute();
        ResultSet rs = pstmt.getGeneratedKeys();
        int key = -1;
        if (rs != null && rs.next()) {
            key = rs.getInt(1);
        }
        return key;
    }

    public void insertPersonOfInterest(String firstName, String lastName, String birthDate) throws SQLException, ParseException{
        PreparedStatement pstmt;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        pstmt = sqlConnection.prepareStatement("INSERT INTO `person_of_interest` (`first_name`,`last_name`,`date_of_birth`) VALUES (?,?,?)");
        Date convertedDate = null;
        // TODO: add else case
        if(birthDate != null && !birthDate.isEmpty()) {
            convertedDate = new Date(sdf.parse(birthDate).getTime());
        }
        pstmt.setString(1, firstName);
        pstmt.setString(2, lastName);
        pstmt.setDate(3, convertedDate);
        pstmt.execute();
    }

    public void linkCategoryToId(String category, int id) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("INSERT INTO `category_has_case` (`category_name`, `case_id`) VALUES (?,?)");
        pstmt.setString(1, category);
        pstmt.setInt(2, id);
        pstmt.execute();
    }

    public void linkPersonToCase(int idP, int idC, String category) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("INSERT INTO `person_of_interest_has_case` (`person_of_interest_id`, `case_id`, `poi_category_name`, `is_linked`) VALUES (?,?,?,?)");
        pstmt.setInt(1, idP);
        pstmt.setInt(2, idC);
        pstmt.setString(3, category);
        pstmt.setBoolean(4, true);
        pstmt.execute();

    }

    public void linkConvictionToCase(int idP, int idC, String startDate, String endDate, String category) throws SQLException, ParseException {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDateStart = new Date(sdf1.parse(startDate).getTime());
        Date convertedDateEnd = null;
        if(!endDate.isEmpty()) {
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
            convertedDateEnd = new Date(sdf2.parse(endDate).getTime());
        }


        PreparedStatement pstmt1;
        pstmt1 = sqlConnection.prepareStatement("INSERT INTO `conviction` (`start_date`, `end_date`, `person_of_interest_id`) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
        pstmt1.setDate(1, convertedDateStart);
        pstmt1.setDate(2, convertedDateEnd);
        pstmt1.setInt(3, idP);
        pstmt1.execute();
        ResultSet rs = pstmt1.getGeneratedKeys();
        int idConv = -1;
        if (rs != null && rs.next()) {
            idConv = rs.getInt(1);
        }
        PreparedStatement pstmt2;
        pstmt2 = sqlConnection.prepareStatement("INSERT INTO `conviction_has_case` (`conviction_id`, `case_id`) VALUES (?,?)");
        pstmt2.setInt(1, idConv);
        pstmt2.setInt(2, idC);
        pstmt2.execute();

        PreparedStatement pstmt3;
        pstmt3 = sqlConnection.prepareStatement("INSERT INTO `category_has_conviction` (`category_name`, `conviction_id`) VALUES (?,?)");
        pstmt3.setString(1, category);
        pstmt3.setInt(2, idConv);
        pstmt3.execute();

    }

    public void unlinkPersonFromCase(int idP, int idC) throws SQLException {
        PreparedStatement pstmt;
        //pstmt = sqlConnection.prepareStatement("UPDATE `person_of_interest_has_case` SET `is_linked`= ? WHERE `person_of_interest_id`= ? AND `case_id`= ?");
//		pstmt.setBoolean(1, false);
        pstmt = sqlConnection.prepareStatement("DELETE FROM `person_of_interest_has_case` WHERE `person_of_interest_id` = ? AND `case_id` = ?");
        pstmt.setInt(1, idP);
        pstmt.setInt(2, idC);
        pstmt.execute();

    }

    public User getUser(String username) throws SQLException {
        if(userExists(username)) {
            PreparedStatement pstmt;
            pstmt = sqlConnection.prepareStatement("SELECT name, image FROM user WHERE name = ?");
            pstmt.setString(1, username);
            pstmt.execute();
            pstmt.getResultSet().next();
            return new User(pstmt.getResultSet());
        }
        else {
            return null;
        }
    }

    public boolean isValidLogin(String username, String password) throws SQLException, NoSuchAlgorithmException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT name FROM user WHERE name = ? AND password = ?");

        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(("crime" + username + password).getBytes());
        pstmt.setString(1, username);
        pstmt.setBytes(2, messageDigest.digest());
        pstmt.execute();

        return pstmt.getResultSet().next();
    }

    public List<Case> getCasesByCategory(String category) throws SQLException {
        List<Category> subCategories = getSubCategories(category);
        List<Case> result = new ArrayList<Case>();
        PreparedStatement pstmt;
        //if it's parent cat
        if(subCategories.isEmpty()) {
            pstmt = sqlConnection.prepareStatement("SELECT c.* FROM `case` c, category_has_case d WHERE c.id = d.case_id and d.category_name = ?");
            pstmt.setString(1, category);
            pstmt.execute();
            ResultSet rs = pstmt.getResultSet();
            while(rs.next()) {
                result.add(new Case(rs));
            }
            // if parent category, list all cases of all subcats
        } else {
            for(Category c : subCategories) {
                pstmt = sqlConnection.prepareStatement("SELECT c.* FROM `case` c, category_has_case d WHERE c.id = d.case_id and d.category_name = ?");
                pstmt.setString(1, c.getName());
                pstmt.execute();
                ResultSet rs = pstmt.getResultSet();
                while(rs.next()) {
                    result.add(new Case(rs));
                }
            }

        }
        return result;


    }

    public List<Case> getCasesByStatus(String status) throws SQLException {
        List<Case> result = new ArrayList<Case>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `case` WHERE status = ?");
        pstmt.setString(1, status);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Case(rs));
        }
        return result;
    }

    public List<Case> getCasesByUser(String username) throws SQLException {
        List<Case> result = new ArrayList<Case>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `case` WHERE `user_name` = ?");
        pstmt.setString(1, username);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Case(rs));
        }
        return result;
    }

    public Case getCaseById(Integer id) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `case` WHERE id = ?");
        pstmt.setInt(1, id);
        pstmt.execute();
        ResultSet result = pstmt.getResultSet();
        result.next();
        return new Case(result);
    }

    public List<Case> getAllCases() throws SQLException {
        List<Case> result = new ArrayList<Case>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `case`");
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Case(rs));
        }
        return result;
    }

    public List<Category> getAllCategories() throws SQLException {
        List<Category> result = new ArrayList<Category>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `category`");
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Category(rs));
        }
        return result;
    }

    public List<Conviction> getAllConvictions() throws SQLException {
        List<Conviction> result = new ArrayList<Conviction>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `conviction`, `case`, `conviction_has_case`, `person_of_interest`, `category_has_conviction` WHERE person_of_interest.id = conviction.person_of_interest_id AND conviction.id = conviction_has_case.conviction_id AND category_has_conviction.conviction_id=conviction.id AND case.id=conviction_has_case.case_id AND case.id=conviction_has_case.case_id");
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Conviction(rs));
        }
        return result;
    }

    public List<Category> getSubCategories(String parent) throws SQLException {
        List<Category> result = new ArrayList<Category>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `category` where parent = ?");
        pstmt.setString(1, parent);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Category(rs));
        }
        return result;
    }

    public Category getCategoryByCaseId(int id) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `category_has_case`, `category`  WHERE category_name = name AND case_id = ?");
        pstmt.setInt(1, id);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        rs.next();
        return new Category(rs);
    }

    public List<Conviction> getConvictionsByCaseId(int id) throws SQLException {
        List<Conviction> result = new ArrayList<Conviction>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM conviction, conviction_has_case, person_of_interest, `case`, category_has_conviction where conviction.person_of_interest_id = person_of_interest.id and conviction_has_case.conviction_id = conviction.id and conviction_has_case.case_id = `case`.id and `case`.id = ? and category_has_conviction.conviction_id = conviction.id");
        pstmt.setInt(1, id);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Conviction(rs));
        }
        return result;
    }

    public List<Conviction> getConvictionsByPersonId(int id) throws SQLException {
        List<Conviction> result = new ArrayList<Conviction>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM conviction, conviction_has_case, person_of_interest, `case`, category_has_conviction where conviction.person_of_interest_id = ? and conviction.person_of_interest_id = person_of_interest.id and conviction_has_case.conviction_id = conviction.id and conviction_has_case.case_id = `case`.id and category_has_conviction.conviction_id = conviction.id");
        pstmt.setInt(1, id);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Conviction(rs));
        }
        return result;
    }

    public List<PersonOfInterest> getAllPersonsOfInterest() throws SQLException {
        List<PersonOfInterest> result = new ArrayList<PersonOfInterest>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `person_of_interest`");
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new PersonOfInterest(rs));
        }
        return result;
    }

    public List<PersonOfInterest> getAllPersonsOfInterestByCaseId(int id) throws SQLException {
        List<PersonOfInterest> result = new ArrayList<PersonOfInterest>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM  `person_of_interest`,`person_of_interest_has_case` WHERE `is_linked` = true AND `person_of_interest_id`=`id` AND `case_id` = ?");
        pstmt.setInt(1, id);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new PersonOfInterest(rs));
        }
        return result;
    }

    public List<PoiCase> getAllCasesByPersonOfInterest(int id) throws SQLException {
        List<PoiCase> result = new ArrayList<PoiCase>();
        PreparedStatement pstmt;
        // TODO: unelegant
        pstmt = sqlConnection.prepareStatement("SELECT first_name, last_name, person_of_interest.id AS person_id, case.id as case_id, title, poi_category_name FROM `case`, `person_of_interest_has_case`, `person_of_interest` WHERE `is_linked` = true AND `person_of_interest_id` = ? AND `case_id` = case.id GROUP BY case.id");
        pstmt.setInt(1, id);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new PoiCase(rs));
        }
        return result;
    }

    public List<PoiCase> getAllPersonsOfInterestByCategory(String cat) throws SQLException {
        List<PoiCase> result = new ArrayList<PoiCase>();
        PreparedStatement pstmt;
        // TODO: unelegant
        pstmt = sqlConnection.prepareStatement("SELECT first_name, last_name, person_of_interest.id AS person_id, case.id as case_id, title, poi_category_name FROM `case`, `person_of_interest_has_case`, `person_of_interest` WHERE `is_linked` = true AND `case_id` = case.id AND poi_category_name = ? GROUP BY case.id");
        pstmt.setString(1, cat);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new PoiCase(rs));
        }
        return result;
    }

    public List<Status> getAllStatus() throws SQLException {
        List<Status> result = new ArrayList<Status>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `status`");
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Status(rs));
        }
        return result;
    }

    public List<PoiCategory> getAllPoiCategories() throws SQLException {
        List<PoiCategory> result = new ArrayList<PoiCategory>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `poi_category`");
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new PoiCategory(rs));
        }
        return result;
    }

    public int getNumberOfCasesByCategory(String category) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT category_name, COUNT(category_name) FROM category_has_case WHERE category_name=? GROUP BY category_name");
        pstmt.setString(1, category);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        if(rs.next() == true ) {
            return rs.getInt("COUNT(category_name)");
        }
        return 0;

    }

    public Note getNoteById(int id) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM note WHERE id = ?");
        pstmt.setInt(1, id);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        rs.next();
        return new Note(rs);
    }

    public List<Note> getNotesByCase(int caseId) throws SQLException {
        List<Note> result = new ArrayList<Note>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM note WHERE case_id = ?");
        pstmt.setInt(1, caseId);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Note(rs));
        }
        return result;
    }

//	public List<Note> getNotesByUser(String userName) throws SQLException {
//		List<Note> result = new ArrayList<Note>();
//		PreparedStatement pstmt;
//		pstmt = sqlConnection.prepareStatement("SELECT * FROM note WHERE user_name = ?");
//		pstmt.setString(1, userName);
//		pstmt.execute();
//		ResultSet rs = pstmt.getResultSet();
//		while(rs.next()) {
//			result.add(new Note(rs));
//		}
//		return result;
//	}

    public List<Note> getNotesByPersonOfInterest(int personOfInterestId) throws SQLException {
        List<Note> result = new ArrayList<Note>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM note WHERE person_of_interest_id = ?");
        pstmt.setInt(1, personOfInterestId);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Note(rs));
        }
        return result;
    }

    public List<Category> getParentCategories() throws SQLException {
        List<Category> result = new ArrayList<Category>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `category` WHERE parent = ?");
        pstmt.setNull(1, java.sql.Types.VARCHAR);
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Category(rs));
        }
        return result;
    }

    public PersonOfInterest getPersonOfInterestById(Integer id) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `person_of_interest` WHERE id = ?");
        pstmt.setInt(1, id);
        pstmt.execute();
        ResultSet result = pstmt.getResultSet();
        result.next();
        return new PersonOfInterest(result);
    }

    public void changeCaseStatus(Integer id, String status) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("UPDATE `case` SET `status` = ? WHERE `id` = ?");
        pstmt.setString(1, status);
        pstmt.setInt(2, id);
        pstmt.execute();
    }

    public void modifyCaseCategory(String name, int id) throws SQLException {
        PreparedStatement pstmt2;
        pstmt2 = sqlConnection.prepareStatement("UPDATE `category_has_case` SET `category_name`= ? WHERE `case_id`= ?");
        pstmt2.setString(1, name);
        pstmt2.setInt(2, id);
        pstmt2.execute();
    }

    public void modifyCase(int id, String title, String date, String time, String description, String location, String category) throws SQLException, ParseException{
        PreparedStatement pstmt;
        PreparedStatement pstmt2;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = null;
        Time convertedTime = null;
        if(!date.isEmpty()) {
            convertedDate = new Date(sdf.parse(date).getTime());
        }
        if(!time.isEmpty()) {
            sdf = new SimpleDateFormat("HH:mm");
            convertedTime = new Time(sdf.parse(time).getTime());
        }
        pstmt = sqlConnection.prepareStatement("UPDATE `case` SET `title`= ?, `date`= ?, `time`= ?, `description`= ?,`location`= ? WHERE `id`= ?");
        pstmt.setString(1, title);
        pstmt.setDate(2, convertedDate);
        pstmt.setTime(3, convertedTime);
        pstmt.setString(4, description);
        pstmt.setString(5, location);
        pstmt.setInt(6, id);


        pstmt2 = sqlConnection.prepareStatement("UPDATE `category_has_case` SET `category_name`= ? WHERE `case_id`= ?");
        pstmt2.setString(1, category);
        pstmt2.setInt(2, id);

        pstmt.execute();
        pstmt2.execute();
    }

    public void modifyPersonOfInterest(int id, String firstName, String lastName, String dateOfBirth) throws SQLException, ParseException{
        PreparedStatement pstmt;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = null;
        if(!dateOfBirth.isEmpty()) {
            convertedDate = new Date(sdf.parse(dateOfBirth).getTime());
        }
        pstmt = sqlConnection.prepareStatement("UPDATE `person_of_interest` SET `first_name`= ?, `last_name`= ?, `date_of_birth`= ? WHERE `id`= ?");
        pstmt.setString(1, firstName);
        pstmt.setString(2, lastName);
        pstmt.setDate(3, convertedDate);
        pstmt.setInt(4, id);
        pstmt.execute();
    }

    public void deleteCaseById(Integer id, Status status) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("DELETE FROM `case` WHERE id = ?");
        pstmt.setInt(1, id);
        pstmt.execute();
    }

    public void deleteSubCategory(String name) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("DELETE FROM `category` WHERE name = ?");
        pstmt.setString(1, name);
        pstmt.execute();
    }
    public void deleteNoteById(int id) throws SQLException {
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("DELETE FROM `note` WHERE id = ?");
        pstmt.setInt(1, id);
        pstmt.execute();
    }

    public List<Case> searchByName(String name, String status) throws SQLException {
        List<Case> result = new ArrayList<Case>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM `case` WHERE  status LIKE ? AND (user_name LIKE ? OR description LIKE ? OR title LIKE ? OR id in(SELECT case_id FROM person_of_interest_has_case, person_of_interest WHERE person_of_interest_id = id AND (first_name LIKE ? OR last_name LIKE ?)))");
        pstmt.setString(1, "%"+status+"%");
        pstmt.setString(2, "%"+name+"%");
        pstmt.setString(3, "%"+name+"%");
        pstmt.setString(4, "%"+name+"%");
        pstmt.setString(5, "%"+name+"%");
        pstmt.setString(6, "%"+name+"%");
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new Case(rs));
        }
        return result;
    }

    public List<Case> getMostRecentCases() throws SQLException {
        List<Case> cases = getAllCases();
        List<Case> remove = new ArrayList<Case>();

        Collections.sort(cases, new Comparator<Case>() {
            public int compare(Case c1, Case c2) {
                if (c1.getDate() == null || c2.getDate() == null)
                    return 1;
                if (c1.getDate().equals(c2.getDate()))
                    return 0;
                else if (c1.getDate().before(c2.getDate()))
                    return 1;
                else
                    return -1;

            }
        });

        for(Case c : cases) {
            if(c.getDate() == null) {
                remove.add(c);
            }
        }
        cases.removeAll(remove);
        return cases;
    }

    public List<Case> getOldestUnsolvedCases() throws SQLException {
        List<Case> cases = getMostRecentCases();
        Collections.reverse(cases);
        List<Case> toBeRemoved = new ArrayList<Case>();
        for(Case c : cases) {
            if(c.getStatus().getStatus().equals("closed")) {
                toBeRemoved.add(c);
            }
        }
        cases.removeAll(toBeRemoved);
        return cases;
    }

    public List<PersonOfInterest> searchPoi(String name) throws SQLException {
        List<PersonOfInterest> result = new ArrayList<PersonOfInterest>();
        PreparedStatement pstmt;
        pstmt = sqlConnection.prepareStatement("SELECT * FROM  `person_of_interest` WHERE `first_name` LIKE ? OR last_name LIKE ?");
        pstmt.setString(1, "%"+name+"%");
        pstmt.setString(2, "%"+name+"%");
        pstmt.execute();
        ResultSet rs = pstmt.getResultSet();
        while(rs.next()) {
            result.add(new PersonOfInterest(rs));
        }
        return result;
    }

    public List<Conviction> searchConviction(String name, String category, String date) throws SQLException {
        List<Conviction> convictions = getAllConvictions();
        List<Conviction> keep = new ArrayList<Conviction>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = null;
        try {
            if(!date.isEmpty()) {
                convertedDate = new Date(sdf.parse(date).getTime());
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        for(Conviction c : convictions) {
            if(c.getFirstName().contains(name) || c.getLastName().contains(name) || c.getTitle().contains(name)) {
                if(c.getCategory().contains(category)) {
                    if(date.isEmpty() || (c.getStartDate().before(convertedDate) && (c.getEndDate() == null || c.getEndDate().after(convertedDate)))){
                        keep.add(c);
                    }
                }
            }
        }

        return keep;
    }
}
