package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Object that represents a registered in user.
 */
public final class PoiCategory {

	private final String name;

	public PoiCategory(final ResultSet rs) throws SQLException {
		this.name = rs.getString("name");
	}
    public PoiCategory(Tuple tuple) {
        this.name = tuple.get(0);
    }
	public PoiCategory(String string) throws SQLException {
		this.name = string;
	}

	public String getName() {
		return name;
	}
	
	public String toString() {
		return name;
	}

}
