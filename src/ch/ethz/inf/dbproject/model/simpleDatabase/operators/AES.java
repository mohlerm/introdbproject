package ch.ethz.inf.dbproject.model.simpleDatabase.operators;
import java.util.Arrays;

import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
 
import javax.crypto.Cipher;
 
public class AES {
  static String IV = "AAAAAAAAAAAAAAAA";
  static String encryptionKey = "oasg.61,2a 12haS";
  final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
  

  public static String encrypt(String plainText) throws Exception {
    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
    SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
    cipher.init(Cipher.ENCRYPT_MODE, key,new IvParameterSpec(IV.getBytes("UTF-8")));
    byte[] toEncrypt = plainText.getBytes("UTF-8");
    if(toEncrypt.length % 16 != 0) {
    	byte[] newArray = new byte[toEncrypt.length + 16 - toEncrypt.length % 16];
    	System.arraycopy(toEncrypt, 0, newArray, 0, toEncrypt.length);
    	toEncrypt = newArray;
    }
    return bytesToHex(cipher.doFinal(toEncrypt));
  }
 
  public static String decrypt(String cipherText) throws Exception{
	byte[] cipherBytes = hexStringToByteArray(cipherText);
    Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding", "SunJCE");
    SecretKeySpec key = new SecretKeySpec(encryptionKey.getBytes("UTF-8"), "AES");
    cipher.init(Cipher.DECRYPT_MODE, key,new IvParameterSpec(IV.getBytes("UTF-8")));
    
    byte[] result = cipher.doFinal(cipherBytes);
    for(int i = 0 ; i < result.length ; i++) {
    	if(result[i] == 0x00) {
    		result = Arrays.copyOf(result, i);
    	}
    }
    
    return new String(result,"UTF-8");
  }
  
  public static byte[] hexStringToByteArray(String s) {
	    int len = s.length();
	    byte[] data = new byte[len / 2];
	    for (int i = 0; i < len; i += 2) {
	        data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
	                             + Character.digit(s.charAt(i+1), 16));
	    }
	    return data;
	}
  
  public static String bytesToHex(byte[] bytes) {
	    char[] hexChars = new char[bytes.length * 2];
	    for ( int j = 0; j < bytes.length; j++ ) {
	        int v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
}
