package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.ArrayList;
import java.util.List;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

/**
 * Created by marcel on 5/25/14.
 */
public class Update extends Operator {
    public final static String REMAIN = "ThisCanBeAnythingMakesUpdateEasier";
    private List<Tuple> buffer = new ArrayList<Tuple>();

    public Update(Operator op, String fileName, String[] columnNames, String[] values) {
    	while(op.moveNext()) {
    		buffer.add(op.current());
    	}
    	Delete deleteOp = new Delete(fileName, columnNames);
    	for(Tuple t : buffer) {
    		deleteOp.delete(t);
    	}
    	Insert insertOp = new Insert(fileName, columnNames);
    	for(Tuple t : buffer) {
    		String [] vals = new String[t.getSchema().getColumnNames().length];
    		for(int i = 0 ; i < t.getSchema().getColumnNames().length; i++) {
    			if(values[i].equals(REMAIN)) {
    				vals[i] = t.get(i);
    			}
    			else {
    				vals[i] = values[i];
    			}
    		}
    		Tuple toInsert = new Tuple(t.getSchema(), vals);
    		insertOp.insert(toInsert);
    	}
    	insertOp.close();
    }

    public boolean moveNext() {
        return false;
    }

}
