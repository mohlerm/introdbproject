package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;


public class Insert {
	public final static String NULL_STRING = "ThisIsOurAwesomeStringWhichGetsReplacedWithEveryNull";
	private final BufferedWriter writer;
	private final String[] columnNames;
	private final String fileName;
	private final String path = "sdb/";
	public final static String TUPLE_SEPERATOR = "thisisourrandomseperator16323";

	/**
	 * Contructs a new Insert operator.
	 * @param fileName file to write tuples to
	 */
	public Insert(
		final String fileName, 
		final String[] columnNames
	) {
		
		this.fileName = fileName;
		this.columnNames = columnNames;

		// write to file
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(path + fileName, true));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException("could not find file " + path + fileName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.writer = writer;
	}

	public void insert(Tuple tuple) {
		String toInsert = "";
		int i = 0;
		
		for(String column : columnNames) {
			String val = tuple.get(i);
			if(val == null) {
				val = NULL_STRING;
			}
			toInsert += val + TUPLE_SEPERATOR;
			i++;
		}
		
		if(!toInsert.isEmpty()) {
			toInsert = toInsert.substring(0, toInsert.length()-TUPLE_SEPERATOR.length());
			try {
				writer.write(AES.encrypt(toInsert) + "\n");
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public void close() {
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String autoincrement(String columnName) {
		int i = 0;
	
		for(String c : columnNames) {
			if(c.equals(columnName)) {
				return autoincrement(i);
			}
			i++;
		}
		return "";
	}
	
	public String autoincrement(int columnIndex) {
        Scan usersScanOperator = new Scan(fileName, columnNames);
		Sort sortOp = new Sort(usersScanOperator, "id", false);
		if(sortOp.moveNext()) {
			return Integer.toString(sortOp.current().getInt(columnIndex) + 1);
		}
		else {
			return "1";
		}
	}
}
