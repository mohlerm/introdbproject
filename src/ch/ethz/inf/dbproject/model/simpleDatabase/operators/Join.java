package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.*;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;

/**
 * An empty sort operator
 * For the purposes of the project, you can consider only string comparisons of
 * the values.
 */
public class Join extends Operator {

	private final Operator left;
	private final Operator right;
	private final int[] leftColumns;
	private final int[] rightColumns;
	
	private List<Tuple> joinBuffer = new ArrayList<Tuple>();
	private TupleSchema schema;
	private boolean firstTime = true;
	private Tuple lastR;
	

	public Join(Operator left, Operator right, int[] leftColumns, int[] rightColumns) {
		this.left = left;
		this.right = right;
		this.lastR = null;
		this.leftColumns = leftColumns;
		this.rightColumns = rightColumns;
	}


	@Override
	public boolean moveNext() {
		boolean match = true;
		int index = 0;
		Tuple r = null;
		
		// Save right table
		if(firstTime) {
			firstTime = false;
			while(right.moveNext()) {
				joinBuffer.add(right.current());
			}
			// Point to first tuple
			if(!left.moveNext()) {
				return false;
			}
		}
		

		do {
			index = 0;
			if(lastR != null) {
				index = joinBuffer.indexOf(lastR) + 1;
			}
			for (int i = index ; i < joinBuffer.size() ; i++) {
				r = joinBuffer.get(i);
				match = true;
				for(int j = 0 ; j < leftColumns.length ; j++) {
					if(!left.current().get(leftColumns[j]).equals(r.get(rightColumns[j]))) {
						match = false;
						break;
					}
				}
				if(match) {
					lastR = r;
					this.schema = new TupleSchema(concat(left.current().getSchema().getColumnNames(), r.getSchema().getColumnNames()));
					current = new Tuple(schema, concat(left.current().getValues(), r.getValues()));
					return true;
				}
			}
			lastR = null;
		} while(left.moveNext());
		return false;
	}
	
	private String[] concat(String[] A, String[] B) {
		   int aLen = A.length;
		   int bLen = B.length;
		   String[] C= new String[aLen+bLen];
		   System.arraycopy(A, 0, C, 0, aLen);
		   System.arraycopy(B, 0, C, aLen, bLen);
		   return C;
		}
}
