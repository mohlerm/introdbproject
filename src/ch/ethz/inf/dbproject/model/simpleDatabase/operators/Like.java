package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

/**
 * Created by marcel on 5/26/14.
 */
public class Like extends Operator{
    private int[] columns;
	private Operator op;
	private String name;
	public Like(Operator op, int columns[], String name) {
    	this.op = op;
    	this.columns = columns;
    	this.name = name;
    }

    public boolean moveNext() {
    	boolean match = false;
        while(op.moveNext()) {
        	match = false;
        	for(int i = 0 ; i < columns.length ; i++) {
        		if(op.current().get(columns[i]).contains(name)) {
        			match = true;
        			break;
        		}
        	}
        	if(match) {
        		current = op.current();
        		return true;
        	}
        }
        return false;
    }
}
