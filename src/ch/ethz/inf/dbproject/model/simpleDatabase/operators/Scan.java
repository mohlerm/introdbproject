package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.io.*;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;


/**
 * The scan operator reads tuples from a file. The lines in the file contain the
 * values of a tuple. The line a comma separated.
 */
public class Scan extends Operator {

	private final TupleSchema schema;
	private final BufferedReader reader;
    private final String path = "sdb/";

	/**
	 * Contructs a new scan operator.
	 * @param fileName file to read tuples from
	 */
	public Scan(
		final String fileName, 
		final String[] columnNames
	) {
		// create schema
		this.schema = new TupleSchema(columnNames);

		// read from file
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(path + fileName));
		} catch (final FileNotFoundException e) {
			throw new RuntimeException("could not find file " + path + fileName);
		}
		this.reader = reader;
	}

	/**
	 * Constructs a new scan operator (mainly for testing purposes).
	 * @param reader reader to read lines from
	 * @param columnNames column names
	 */
	public Scan(
		final Reader reader, 
		final String[] columnNames
	) {
		this.reader = new BufferedReader(reader);
		this.schema = new TupleSchema(columnNames);
	}

	@Override
	public boolean moveNext() {
		
		try {
			String line = reader.readLine();
			if (line != null) {
				line = AES.decrypt(line);
				String [] decrypted = line.split(Insert.TUPLE_SEPERATOR, -1);
				String [] fixed = new String[decrypted.length];
				int i = 0;
				for (String s : decrypted) {
					if(s == null) {
						fixed[i] = "";
					}
					else if(s.equals(Insert.NULL_STRING)) {
						fixed[i] = "";
					}
					else {
						fixed[i] = decrypted[i];
					}
					i++;
				}
				current = new Tuple(schema, fixed);
				return true;
			}
			else {
				reader.close();
				return false;
			}
		} catch (final IOException e) {
			throw new RuntimeException("could not read: " + this.reader + ". Error is " + e);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
