package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;


public class Delete {
	public final static String ANY = "ThisCanBeAnythingMakesDeleteEasier";
	private final String[] columnNames;
	private final String fileName;
	private final String path = "sdb/";

	/**
	 * Contructs a new Insert operator.
	 * @param fileName file to write tuples to
	 */
	public Delete(
		final String fileName, 
		final String[] columnNames
	) {
		
		this.fileName = fileName;
		this.columnNames = columnNames;

	}

	public void delete(Tuple tuple) {
		boolean match = true;
        
        
        Scan scanOperator = new Scan(fileName, columnNames);
        Insert insertOp = new Insert(fileName + ".tmp", columnNames);
        
        while(scanOperator.moveNext()) {
        	match = true;
        	for(int i = 0 ; i < tuple.getSchema().getColumnNames().length ; i++) {
        		if(tuple.get(i) == null) {
        			if(scanOperator.current() != null) {
        				match = false;
        			}
        		}
        		else if(!tuple.get(i).equals(ANY)) {
        			if(scanOperator.current().get(i) == null) {
        				match = false;
        			}
        			else if(!tuple.get(i).equals(scanOperator.current().get(i))) {
        				match = false;
        			}
        		}
        	}
        	if(!match) {
        		insertOp.insert(scanOperator.current());
        	}
        }
        insertOp.close();
        
        File file1 = new File(path + fileName);
        File file2 = new File(path + fileName + ".delete");
        File file3 = new File(path + fileName + ".tmp");
        
        file1.renameTo(file2);
        file3.renameTo(file1);
        file2.delete();
	}
	
}
