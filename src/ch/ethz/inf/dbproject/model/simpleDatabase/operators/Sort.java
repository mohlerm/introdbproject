package ch.ethz.inf.dbproject.model.simpleDatabase.operators;

import java.util.*;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

/**
 * An empty sort operator
 * For the purposes of the project, you can consider only string comparisons of
 * the values.
 */
public class Sort extends Operator implements Comparator<Tuple> {

	private final Operator op;
	private final String column;
	private final boolean ascending;
	private final ArrayList<Tuple> sortBuffer;
	private int lastIndex;
	
	public Sort(
		final Operator op,
		final String column,
		final boolean ascending
	) {
		this.op = op;
		this.column = column;
		this.ascending = ascending;
		this.sortBuffer = new ArrayList<Tuple>();
		this.lastIndex = 0;
	}
	
	@Override
	public final int compare(
		final Tuple l, 
		final Tuple r
	) {
		
		final int columnIndex = l.getSchema().getIndex(this.column);
		
		//final int result =
			//l.get(columnIndex).compareToIgnoreCase(r.get(columnIndex));

        final int result;
        if (l.getInt(columnIndex) < (r.getInt(columnIndex))) {
            result =  -1;
        }
        else if (l.getInt(columnIndex) > (r.getInt(columnIndex))) {
            result =  1;
        } else {
            result =  0;
        }

		if (this.ascending) {
			return result;
		} else {
			return -result;
		}
	}

	@Override
	public boolean moveNext() {

        // a) if this is the first call:
        if(lastIndex == 0) {
            //   1) fetch _all_ tuples from this.op and store them in sort buffer
            while (this.op.moveNext()) {
                this.sortBuffer.add(this.op.current());
            }
            //   2) sort the buffer
            Collections.sort(this.sortBuffer, this);
            //   3) set the current tuple to the first one in the sort buffer and
            //      remember you are at offset 0
            if(sortBuffer.size() > 0) {
                current = this.sortBuffer.get(0);
                lastIndex += 1;
                return true;
            }
            else {
            	return false;
            }
        // b) if this is not the first call
        } else {
            //   1) increase the offset and if it is valid fetch the next tuple
        	if(lastIndex < sortBuffer.size()) {
        		current = this.sortBuffer.get(lastIndex);
        		lastIndex += 1;
        		return true;
        	}
        }
		return false;
	}

	
}
