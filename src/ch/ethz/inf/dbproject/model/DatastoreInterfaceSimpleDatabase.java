package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.database.MySQLConnection;
import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.TupleSchema;
import ch.ethz.inf.dbproject.model.simpleDatabase.operators.*;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This class should be the interface between the web application
 * and the database. Keeping all the data-access methods here
 * will be very helpful for part 2 of the project.
 */
public final class DatastoreInterfaceSimpleDatabase implements DatastoreInterface {

    @SuppressWarnings("unused")
    private Connection sqlConnection;
    //private static DatastoreInterfaceSimpleDatabase instance = null;

    public DatastoreInterfaceSimpleDatabase() {
        this.sqlConnection = MySQLConnection.getInstance().getConnection();
    }

//    public static DatastoreInterfaceSimpleDatabase getInstance() {
//        if(instance == null) {
//            instance = new DatastoreInterfaceSimpleDatabase();
//        }
//        return instance;
//    }

    /*
        >> user
        String[] columnNames = {"name", "password", "image"};
        Operator myOperator = new Operator("user.sdb", columnNames);

        >> category
        String[] columnNames = {"name", "parent"};
        Operator myOperator = new Operator("category.sdb", columnNames);

        >> category_has_case
        String[] columnNames = {"category_name", "case_id"};
        Operator myOperator = new Operator("category_has_case.sdb", columnNames);

        >> category_has_conviction
        String[] columnNames = {"category_name", "conviction_id"};
        Operator myOperator = new Operator("category_has_conviction.sdb", columnNames);

        >> conviction
        String[] columnNames = {"id", "start_date", "end_date", "person_of_interest_id};
        Operator myOperator = new Operator("conviction.sdb", columnNames);

        >> conviction_has_case
        String[] columnNames = {"conviction_id", "case_id"};
        Operator myOperator = new Operator("conviction_has_case.sdb", columnNames);

        >> case
        String[] columnNames = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        Operator myOperator = new Operator("case.sdb", columnNames);

        >> note
        String[] columnNames = {"id", "text", "case_id", "person_of_interest_id", "user_name"};
        Operator myOperator = new Operator("note.sdb", columnNames);

        >> status
        String[] columnNames = {"status"};
        Operator myOperator = new Operator("status.sdb", columnNames);

        >> person_of_interest
        String[] columnNames = {"id", "first_name", "last_name", "date_of_birth"};
        Operator myOperator = new Operator("person_of_interest.sdb", columnNames);

        >> person_of_interest_has_case
        String[] columnNames = {"person_of_interest_id", "case_id", "poi_category_name", "is_linked"};
        Operator myOperator = new Operator("person_of_interest_has_case.sdb", columnNames);

        >> poi_category
        String[] columnNames = {"name"};
        Operator myOperator = new Operator("poi_category.sdb", columnNames);
     */


    public boolean userExists(String username) throws SQLException {
        //PreparedStatement pstmt = sqlConnection.prepareStatement("SELECT name FROM user WHERE name = ?");
        String[] columnNames = {"name", "password", "image"};
        Scan usersScanOperator = new Scan("user.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "name", username);
        return selectOp.moveNext();
    }

    public void insertUser(String username, String password, byte[] picture) throws NoSuchAlgorithmException, SQLException {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(("crime" + username + password).getBytes());
        //pstmt = sqlConnection.prepareStatement("INSERT INTO user (`name`, `password`, `image`) VALUES (?,?,?)");
        String[] columnNames = {"name", "password", "image"};
        String[] values = {username, AES.bytesToHex(messageDigest.digest()), AES.bytesToHex(picture)};
        Insert userInsertOperator = new Insert("user.sdb", columnNames);
        Tuple tuple = new Tuple(new TupleSchema(columnNames), values);
        userInsertOperator.insert(tuple);
        userInsertOperator.close();
    }

    public void insertCategory(String parent, String name) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("INSERT INTO category (`parent`, `name`) VALUES (?,?)");
        String[] columnNames = {"name", "parent"};
        String[] values = new String[2];
        values[0] = name;
        values[1]= parent;
        Insert userInsertOperator = new Insert("category.sdb", columnNames);
        Tuple tuple = new Tuple(new TupleSchema(columnNames), values);
        userInsertOperator.insert(tuple);
        userInsertOperator.close();

    }

    public void insertNote(String text, String userName, String caseId, String personOfInterestId) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("INSERT INTO note (`text`, `case_id`, `person_of_interest_id`, `user_name`) VALUES (?,?,?,?)");
        String[] columnNames = {"id", "text", "case_id", "person_of_interest_id", "user_name"};
        String[] values = new String[5];
        values[1] = text;
        values[2] = caseId;
        values[3] = personOfInterestId;
        values[4] = userName;
        Insert userInsertOperator = new Insert("note.sdb", columnNames);
        values[0] = userInsertOperator.autoincrement("id");
        Tuple tuple = new Tuple(new TupleSchema(columnNames), values);
        userInsertOperator.insert(tuple);
        userInsertOperator.close();
    }

    public int insertCase(String title, String date, String time, String description, String location, String user_name) throws SQLException, ParseException {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date convertedDate = null;
//        Time convertedTime = null;
//        if(!date.isEmpty()) {
//            convertedDate = new Date(sdf.parse(date).getTime());
//        }
//        if(!time.isEmpty()) {
//            sdf = new SimpleDateFormat("HH:mm");
//            convertedTime = new Time(sdf.parse(time).getTime());
//        }

        //pstmt = sqlConnection.prepareStatement("INSERT INTO `case` (`title`,`date`,`time`,`description`,`location`,`user_name`,`status`) VALUES (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
        String[] columnNames = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        String[] values = new String[8];
        values[1] = title;
        values[2] = date;
        values[3] = time;
        values[4] = description;
        values[5] = location;
        values[6] = user_name;
        values[7] = "open";
        Insert userInsertOperator = new Insert("case.sdb", columnNames);
        values[0] = userInsertOperator.autoincrement("id");
        Tuple tuple = new Tuple(new TupleSchema(columnNames), values);
        userInsertOperator.insert(tuple);
        userInsertOperator.close();
        return Integer.parseInt(values[0]);
    }

    public void insertPersonOfInterest(String firstName, String lastName, String birthDate) throws SQLException, ParseException{
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        //pstmt = sqlConnection.prepareStatement("INSERT INTO `person_of_interest` (`first_name`,`last_name`,`date_of_birth`) VALUES (?,?,?)");
        //Date convertedDate = null;
        String[] columnNames = {"id", "first_name", "last_name", "date_of_birth"};
        String[] values = new String[4];
//        if(birthDate != null && !birthDate.isEmpty()) {
//            convertedDate = new Date(sdf.parse(birthDate).getTime());
//        }
        values[1] = firstName;
        values[2] = lastName;
        values[3] = birthDate;
        Insert userInsertOperator = new Insert("person_of_interest.sdb", columnNames);
        values[0] = userInsertOperator.autoincrement("id");
        Tuple tuple = new Tuple(new TupleSchema(columnNames), values);
        userInsertOperator.insert(tuple);
        userInsertOperator.close();
    }

    public void linkCategoryToId(String category, int id) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("INSERT INTO `category_has_case` (`category_name`, `case_id`) VALUES (?,?)");
        String[] columnNames = {"category_name", "case_id"};
        String[] values = new String[2];
        values[0] = category;
        values[1] = String.valueOf(id);
        Insert userInsertOperator = new Insert("category_has_case.sdb", columnNames);
        Tuple tuple = new Tuple(new TupleSchema(columnNames), values);
        userInsertOperator.insert(tuple);
        userInsertOperator.close();
    }

    public void linkPersonToCase(int idP, int idC, String category) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("INSERT INTO `person_of_interest_has_case` (`person_of_interest_id`, `case_id`, `poi_category_name`, `is_linked`) VALUES (?,?,?,?)");
        String[] columnNames = {"person_of_interest_id", "case_id", "poi_category_name", "is_linked"};
        String[] values = new String[4];
        values[0] = String.valueOf(idP);
        values[1] = String.valueOf(idC);
        values[2] = category;
        values[3] = String.valueOf(true);
        Insert userInsertOperator = new Insert("person_of_interest_has_case.sdb", columnNames);
        Tuple tuple = new Tuple(new TupleSchema(columnNames), values);
        userInsertOperator.insert(tuple);
        userInsertOperator.close();
    }

    public void linkConvictionToCase(int idP, int idC, String startDate, String endDate, String category) throws SQLException, ParseException {
//        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
//        Date convertedDateStart = new Date(sdf1.parse(startDate).getTime());
//        Date convertedDateEnd = null;
//        if(!endDate.isEmpty()) {
//            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
//            convertedDateEnd = new Date(sdf2.parse(endDate).getTime());
//        }

        //pstmt1 = sqlConnection.prepareStatement("INSERT INTO `conviction` (`start_date`, `end_date`, `person_of_interest_id`) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
        String[] columnNames1 = {"id", "start_date", "end_date", "person_of_interest_id"};
        String[] values1 = new String[4];
        values1[1] = startDate;
        values1[2] = endDate;
        values1[3] = String.valueOf(idP);
        Insert userInsertOperator1 = new Insert("conviction.sdb", columnNames1);
        values1[0] = userInsertOperator1.autoincrement("id");
        Tuple tuple1 = new Tuple(new TupleSchema(columnNames1), values1);
        userInsertOperator1.insert(tuple1);
        userInsertOperator1.close();
        String idConv = values1[0];

        //pstmt2 = sqlConnection.prepareStatement("INSERT INTO `conviction_has_case` (`conviction_id`, `case_id`) VALUES (?,?)");
        String[] columnNames2 = {"conviction_id", "case_id"};
        String[] values2 = new String[2];
        values2[0] = idConv;
        values2[1] = String.valueOf(idC);
        Insert userInsertOperator2 = new Insert("conviction_has_case.sdb", columnNames2);
        Tuple tuple2 = new Tuple(new TupleSchema(columnNames2), values2);
        userInsertOperator2.insert(tuple2);
        userInsertOperator2.close();

        //pstmt3 = sqlConnection.prepareStatement("INSERT INTO `category_has_conviction` (`category_name`, `conviction_id`) VALUES (?,?)");
        String[] columnNames3 = {"category_name", "conviction_id"};
        String[] values3 = new String[2];
        values3[0] = category;
        values3[1] = idConv;
        Insert userInsertOperator3 = new Insert("category_has_conviction.sdb", columnNames3);
        Tuple tuple3 = new Tuple(new TupleSchema(columnNames3), values3);
        userInsertOperator3.insert(tuple3);
        userInsertOperator3.close();

    }

    public void unlinkPersonFromCase(int idP, int idC) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("UPDATE `person_of_interest_has_case` SET `is_linked`= ? WHERE `person_of_interest_id`= ? AND `case_id`= ?");
//		pprivatestmt.setBoolean(1, false);
        //pstmt = sqlConnection.prepareStatement("DELETE FROM `person_of_interest_has_case` WHERE `person_of_interest_id` = ? AND `case_id` = ?");
        String[] columnNames = {"person_of_interest_id", "case_id", "poi_category_name", "is_linked"};
        String[] values = new String[4];
        values[0] = String.valueOf(idP);
        values[1] = String.valueOf(idC);
        values[2] = Delete.ANY;
        values[3] = Delete.ANY;
        Delete userDeleteOperator = new Delete("person_of_interest_has_case.sdb", columnNames);
        Tuple tuple = new Tuple(new TupleSchema(columnNames), values);
        userDeleteOperator.delete(tuple);
    }

    public User getUser(String username) throws SQLException {
        if(userExists(username)) {
            //pstmt = sqlConnection.prepareStatement("SELECT name, image FROM user WHERE name = ?");
            String[] columnNames = {"name", "password", "image"};
            Scan usersScanOperator = new Scan("user.sdb", columnNames);
            Select<String> selectOp = new Select<String>(usersScanOperator, "name", username);
            selectOp.moveNext();
            return new User(selectOp.current());
        }
        else {
            return null;
        }
    }

    public boolean isValidLogin(String username, String password) throws SQLException, NoSuchAlgorithmException {

        //pstmt = sqlConnection.prepareStatement("SELECT name FROM user WHERE name = ? AND password = ?");

        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        messageDigest.update(("crime" + username + password).getBytes());
        String[] columnNames = {"name", "password"};
        Scan usersScanOperator = new Scan("user.sdb", columnNames);
        Select<String> selectOp1 = new Select<String>(usersScanOperator, "name", username);
        Select<String> selectOp2 = new Select<String>(selectOp1, "password", AES.bytesToHex(messageDigest.digest()));

        return selectOp2.moveNext();
    }

    public List<Case> getCasesByCategory(String category) throws SQLException {
        List<Category> subCategories = getSubCategories(category);
        List<Case> result = new ArrayList<Case>();
        //if it's parent cat
        if(subCategories.isEmpty()) {
            //pstmt = sqlConnection.prepareStatement("SELECT c.* FROM `case` c, category_has_case d WHERE c.id = d.case_id and d.category_name = ?");
            String[] columnNames1 = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
            String[] columnNames2 = {"category_name", "case_id"};
            Scan op1 = new Scan("case.sdb", columnNames1);
            Scan op2 = new Scan("category_has_case.sdb", columnNames2);
            int[] left = {0};
            int[] right = {1};
            Join op3 = new Join(op1, op2, left, right);
            Select<String> selectOp1 = new Select<String>(op3, "category_name", category);
            while(selectOp1.moveNext()) {
                result.add(new Case(selectOp1.current()));
            }
            // if parent category, list all cases of all subcats
        } else {
            for(Category c : subCategories) {
                //pstmt = sqlConnection.prepareStatement("SELECT c.* FROM `case` c, category_has_case d WHERE c.id = d.case_id and d.category_name = ?");
                String[] columnNames1 = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
                String[] columnNames2 = {"category_name", "case_id"};
                Scan op1 = new Scan("case.sdb", columnNames1);
                Scan op2 = new Scan("category_has_case.sdb", columnNames2);
                int[] left = {0};
                int[] right = {1};
                Join op3 = new Join(op1, op2, left, right);
                Select<String> selectOp1 = new Select<String>(op3, "category_name", c.getName());
                while(selectOp1.moveNext()) {
                    result.add(new Case(selectOp1.current()));
                }
            }

        }
        return result;


    }

    public List<Case> getCasesByStatus(String status) throws SQLException {
        List<Case> result = new ArrayList<Case>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `case` WHERE status = ?");
        String[] columnNames = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        Scan usersScanOperator = new Scan("case.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "status", status);
        while(selectOp.moveNext()) {
            result.add(new Case(selectOp.current()));
        }
        return result;
    }

    public List<Case> getCasesByUser(String username) throws SQLException {
        List<Case> result = new ArrayList<Case>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `case` WHERE `user_name` = ?");
        String[] columnNames = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        Scan usersScanOperator = new Scan("case.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "user_name", username);
        while(selectOp.moveNext()) {
            result.add(new Case(selectOp.current()));
        }
        return result;
    }

    public Case getCaseById(Integer id) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `case` WHERE id = ?");
        String[] columnNames = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        Scan usersScanOperator = new Scan("case.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "id", Integer.toString(id));
        selectOp.moveNext();
        return new Case(selectOp.current());
    }

    public List<Case> getAllCases() throws SQLException {
        List<Case> result = new ArrayList<Case>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `case`");
        String[] columnNames = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        Scan usersScanOperator = new Scan("case.sdb", columnNames);
        Sort sortOp = new Sort(usersScanOperator, "id", true);
        while(sortOp.moveNext()) {
            result.add(new Case(sortOp.current()));
        }
        return result;
    }

    public List<Category> getAllCategories() throws SQLException {
        List<Category> result = new ArrayList<Category>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `category`");
        String[] columnNames = {"name", "parent"};
        Scan usersScanOperator = new Scan("category.sdb", columnNames);
        while(usersScanOperator.moveNext()) {
            result.add(new Category(usersScanOperator.current()));
        }
        return result;
    }

    public List<Conviction> getAllConvictions() throws SQLException {
        List<Conviction> result = new ArrayList<Conviction>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `conviction`, `case`, `conviction_has_case`, `person_of_interest`, `category_has_conviction` WHERE
        // person_of_interest.id = conviction.person_of_interest_id AND
        // conviction.id = conviction_has_case.conviction_id AND
        // category_has_conviction.conviction_id=conviction.id AND
        // case.id=conviction_has_case.case_id AND case.id=conviction_has_case.case_id");
        String[] columnPOI = {"id", "first_name", "last_name", "date_of_birth"};
        String[] columnConv = {"id", "start_date", "end_date", "person_of_interest_id"};
        String[] columnConvHC = {"conviction_id", "case_id"};
        String[] columnCHC = {"category_name", "conviction_id"};
        String[] columnCase = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        Scan op1 = new Scan("conviction.sdb", columnConv);
        Scan op2 = new Scan("case.sdb", columnCase);
        Scan op3 = new Scan("conviction_has_case.sdb", columnConvHC);
        Scan op4 = new Scan("person_of_interest.sdb", columnPOI);
        Scan op5 = new Scan("category_has_conviction.sdb", columnCHC);
        int[] left1 = {0};
        int[] right1 = {3};
        Join join1 = new Join(op4, op1, left1, right1);
        int[] left2 = {4};
        int[] right2 = {0};
        Join join2 = new Join(join1, op3, left2, right2);
        int[] left3 = {4};
        int[] right3 = {1};
        Join join3 = new Join(join2, op5, left3, right3);
        int[] left4 = {9};
        int[] right4 = {0};
        Join join4 = new Join(join3, op2, left4, right4);
        while(join4.moveNext()) {
            result.add(new Conviction(join4.current()));
        }
        return result;
    }

    public List<Category> getSubCategories(String parent) throws SQLException {
        List<Category> result = new ArrayList<Category>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `category` where parent = ?");
        String[] columnNames = {"name", "parent"};
        Scan usersScanOperator = new Scan("category.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "parent", parent);
        while(selectOp.moveNext()) {
            result.add(new Category(selectOp.current()));
        }
        return result;
    }

    public Category getCategoryByCaseId(int id) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `category_has_case`, `category`  WHERE category_name = name AND case_id = ?");
        String[] columnNames = {"category_name", "case_id"};
        Scan usersScanOperator = new Scan("category_has_case.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "case_id", Integer.toString(id));
        selectOp.moveNext();
        return new Category(selectOp.current());
    }

    public List<Conviction> getConvictionsByCaseId(int id) throws SQLException {
        List<Conviction> result = new ArrayList<Conviction>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM conviction, conviction_has_case, person_of_interest, `case`, category_has_conviction where
        // conviction.person_of_interest_id = person_of_interest.id and
        // conviction_has_case.conviction_id = conviction.id and
        // conviction_has_case.case_id = `case`.id and
        // `case`.id = ? and
        // category_has_conviction.conviction_id = conviction.id");
        String[] columnPOI = {"id", "first_name", "last_name", "date_of_birth"};
        String[] columnConv = {"id", "start_date", "end_date", "person_of_interest_id"};
        String[] columnConvHC = {"conviction_id", "case_id"};
        String[] columnCHC = {"category_name", "conviction_id"};
        String[] columnCase = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        Scan op1 = new Scan("conviction.sdb", columnConv);
        Scan op2 = new Scan("case.sdb", columnCase);
        Scan op3 = new Scan("conviction_has_case.sdb", columnConvHC);
        Scan op4 = new Scan("person_of_interest.sdb", columnPOI);
        Scan op5 = new Scan("category_has_conviction.sdb", columnCHC);
        int[] left1 = {0};
        int[] right1 = {3};
        Join join1 = new Join(op4, op1, left1, right1);
        int[] left2 = {4};
        int[] right2 = {0};
        Join join2 = new Join(join1, op3, left2, right2);
        int[] left3 = {4};
        int[] right3 = {1};
        Join join3 = new Join(join2, op5, left3, right3);
        Select<String> select = new Select<>(op2, "id", String.valueOf(id));
        int[] left4 = {9};
        int[] right4 = {0};
        Join join4 = new Join(join3, select, left4, right4);

        while(join4.moveNext()) {
            result.add(new Conviction(join4.current()));
        }
        return result;
    }

    public List<Conviction> getConvictionsByPersonId(int id) throws SQLException {
        List<Conviction> result = new ArrayList<Conviction>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM conviction, conviction_has_case, person_of_interest, `case`, category_has_conviction where
        // conviction.person_of_interest_id = ? and
        // conviction.person_of_interest_id = person_of_interest.id and
        // conviction_has_case.conviction_id = conviction.id and
        // conviction_has_case.case_id = `case`.id and
        // category_has_conviction.conviction_id = conviction.id");
        String[] columnPOI = {"id", "first_name", "last_name", "date_of_birth"};
        String[] columnConv = {"id", "start_date", "end_date", "person_of_interest_id"};
        String[] columnConvHC = {"conviction_id", "case_id"};
        String[] columnCHC = {"category_name", "conviction_id"};
        String[] columnCase = {"id", "title", "date", "time", "description", "location", "user_name", "status"};

        Scan op1 = new Scan("conviction.sdb", columnConv);
        Scan op2 = new Scan("case.sdb", columnCase);
        Scan op3 = new Scan("conviction_has_case.sdb", columnConvHC);
        Scan op4 = new Scan("person_of_interest.sdb", columnPOI);
        Scan op5 = new Scan("category_has_conviction.sdb", columnCHC);
        Select<String> select = new Select<>(op1, "person_of_interest_id", String.valueOf(id));

        int[] left1 = {0};
        int[] right1 = {3};
        Join join1 = new Join(op4, select, left1, right1);
        int[] left2 = {4};
        int[] right2 = {0};
        Join join2 = new Join(join1, op3, left2, right2);
        int[] left3 = {4};
        int[] right3 = {1};
        Join join3 = new Join(join2, op5, left3, right3);
        int[] left4 = {9};
        int[] right4 = {0};
        Join join4 = new Join(join3, op2, left4, right4);
        while(join4.moveNext()) {
            result.add(new Conviction(join4.current()));
        }
        return result;
    }

    public List<PersonOfInterest> getAllPersonsOfInterest() throws SQLException {
        List<PersonOfInterest> result = new ArrayList<PersonOfInterest>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `person_of_interest`");
        String[] columnNames = {"id", "first_name", "last_name", "date_of_birth"};
        Scan usersScanOperator = new Scan("person_of_interest.sdb", columnNames);
        Sort sortOp = new Sort(usersScanOperator, "id", true);
        while(sortOp.moveNext()) {
            result.add(new PersonOfInterest(sortOp.current()));
        }
        return result;
    }

    public List<PersonOfInterest> getAllPersonsOfInterestByCaseId(int id) throws SQLException {
        List<PersonOfInterest> result = new ArrayList<PersonOfInterest>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM  `person_of_interest`,`person_of_interest_has_case`
        // WHERE `is_linked` = true AND
        // `person_of_interest_id`=`id` AND `case_id` = ?");
        String[] columnPOI = {"id", "first_name", "last_name", "date_of_birth"};
        String[] columnPOIHC = {"person_of_interest_id", "case_id", "poi_category_name", "is_linked"};
        Scan op1 = new Scan("person_of_interest.sdb", columnPOI);
        Scan op2 = new Scan("person_of_interest_has_case.sdb", columnPOIHC);
        int[] left1 = {0};
        int[] right1 = {0};
        Join join1 = new Join(op1, op2, left1, right1);
        Select<String> selectOp= new Select<>(join1, "case_id", String.valueOf(id));
        while(selectOp.moveNext()) {
            result.add(new PersonOfInterest(selectOp.current()));
        }
        return result;
    }

    public List<PoiCase> getAllCasesByPersonOfInterest(int id) throws SQLException {
        List<PoiCase> result = new ArrayList<PoiCase>();
        //pstmt = sqlConnection.prepareStatement("SELECT first_name, last_name,
        // person_of_interest.id AS person_id, case.id as case_id, title, poi_category_name
        // FROM `case`, `person_of_interest_has_case`, `person_of_interest`
        // WHERE `is_linked` = true
        // AND `person_of_interest_id` = ?
        // AND `case_id` = case.id GROUP BY case.id");
        String[] columnCase = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        String[] columnPOIHC = {"person_of_interest_id", "case_id", "poi_category_name", "is_linked"};
        String[] columnPOI = {"id", "first_name", "last_name", "date_of_birth"};
        Scan op1 = new Scan("person_of_interest_has_case.sdb", columnPOIHC);
        Scan op2 = new Scan("case.sdb", columnCase);
        Scan op3 = new Scan("person_of_interest.sdb", columnPOI);
        Select<String> selectOp1= new Select<>(op1, "is_linked", String.valueOf(true));
        Select<String> selectOp2= new Select<>(selectOp1, "person_of_interest_id", String.valueOf(id));
        int[] left1 = {1};
        int[] right1 = {0};
        Join join1 = new Join(selectOp2, op2, left1, right1);
        int[] left2 = {0};
        int[] right2 = {0};
        Join join2 = new Join(join1, op3, left2, right2);
        while(join2.moveNext()) {
            result.add(new PoiCase(join2.current()));
        }
        return result;
    }

    public List<PoiCase> getAllPersonsOfInterestByCategory(String cat) throws SQLException {
        List<PoiCase> result = new ArrayList<PoiCase>();
        //pstmt = sqlConnection.prepareStatement("SELECT first_name, last_name, person_of_interest.id AS person_id, case.id as case_id, title, poi_category_name
        // FROM `case`, `person_of_interest_has_case`, `person_of_interest` WHERE
        // `is_linked` = true AND
        // `case_id` = case.id AND
        // poi_category_name = ? GROUP BY case.id");
        String[] columnPOIHC = {"person_of_interest_id", "case_id", "poi_category_name", "is_linked"};
        String[] columnCase = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        String[] columnPOI = {"id", "first_name", "last_name", "date_of_birth"};
        Scan op1 = new Scan("person_of_interest_has_case.sdb", columnPOIHC);
        Scan op2 = new Scan("case.sdb", columnCase);
        Scan op3 = new Scan("person_of_interest.sdb", columnPOI);
        Select<String> selectOp1= new Select<>(op1, "is_linked", String.valueOf(true));
        Select<String> selectOp2= new Select<>(selectOp1, "poi_category_name", cat);
        int[] left1 = {1};
        int[] right1 = {0};
        Join join1 = new Join(selectOp2, op2, left1, right1);
        int[] left2 = {0};
        int[] right2 = {0};
        Join join2 = new Join(join1, op3, left2, right2);
        while(join2.moveNext()) {
            result.add(new PoiCase(join2.current()));
        }
        return result;
    }

    public List<Status> getAllStatus() throws SQLException {
        List<Status> result = new ArrayList<Status>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `status`");
        String[] columnNames = {"status"};
        Scan usersScanOperator = new Scan("status.sdb", columnNames);
        //Sort sortOp = new Sort(usersScanOperator, "status", false);
        //Select<String> sortOp= new Select<String>(usersScanOperator, "status", "open");
        while(usersScanOperator.moveNext()) {
            result.add(new Status(usersScanOperator.current()));
        }
        return result;
    }

    public List<PoiCategory> getAllPoiCategories() throws SQLException {
        List<PoiCategory> result = new ArrayList<PoiCategory>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `poi_category`");
        String[] columnNames = {"name"};
        Scan usersScanOperator = new Scan("poi_category.sdb", columnNames);
        //Sort sortOp = new Sort(usersScanOperator, "name", false);
        while(usersScanOperator.moveNext()) {
            result.add(new PoiCategory(usersScanOperator.current()));
        }
        return result;
    }

    public int getNumberOfCasesByCategory(String category) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("SELECT category_name, COUNT(category_name) FROM category_has_case WHERE category_name=? GROUP BY category_name");
        List<Case> list = new ArrayList<>(getCasesByCategory(category));
//        ResultSet rs = pstmt.getResultSet();
//        if(rs.next() == true ) {
//            return rs.getInt("COUNT(category_name)");
//        }
        return list.size();

    }

    public Note getNoteById(int id) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM note WHERE id = ?");
        String[] columnNames = {"id", "text", "case_id", "person_of_interest_id", "user_name"};
        Scan usersScanOperator = new Scan("note.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "id", Integer.toString(id));
        selectOp.moveNext();
        return new Note(selectOp.current());
    }

    public List<Note> getNotesByCase(int caseId) throws SQLException {
        List<Note> result = new ArrayList<Note>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM note WHERE case_id = ?");
        String[] columnNames = {"id", "text", "case_id", "person_of_interest_id", "user_name"};
        Scan usersScanOperator = new Scan("note.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "case_id", Integer.toString(caseId));
        while(selectOp.moveNext()) {
            result.add(new Note(selectOp.current()));
        }
        return result;
    }

//	public List<Note> getNotesByUser(String userName) throws SQLException {
//		List<Note> result = new ArrayList<Note>();
//		PreparedStatement pstmt;
//		pstmt = sqlConnection.prepareStatement("SELECT * FROM note WHERE user_name = ?");
//		pstmt.setString(1, userName);
//		pstmt.execute();
//		ResultSet rs = pstmt.getResultSet();
//		while(rs.next()) {
//			result.add(new Note(rs));
//		}
//		return result;
//	}

    public List<Note> getNotesByPersonOfInterest(int personOfInterestId) throws SQLException {
        List<Note> result = new ArrayList<Note>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM note WHERE person_of_interest_id = ?");
        String[] columnNames = {"id", "text", "case_id", "person_of_interest_id", "user_name"};
        Scan usersScanOperator = new Scan("note.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "person_of_interest_id", Integer.toString(personOfInterestId));
        while(selectOp.moveNext()) {
            result.add(new Note(selectOp.current()));
        }
        return result;
    }

    public List<Category> getParentCategories() throws SQLException {
        List<Category> result = new ArrayList<Category>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `category` WHERE parent = ?");
        String[] columnNames = {"name", "parent"};
        Scan usersScanOperator = new Scan("category.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "parent", "null");
        while(selectOp.moveNext()) {
            result.add(new Category(selectOp.current()));
        }
        return result;
    }

    public PersonOfInterest getPersonOfInterestById(Integer id) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `person_of_interest` WHERE id = ?");
        String[] columnNames = {"id", "first_name", "last_name", "date_of_birth"};
        Scan usersScanOperator = new Scan("person_of_interest.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "id", Integer.toString(id));
        selectOp.moveNext();
        return new PersonOfInterest(selectOp.current());
    }

    public void changeCaseStatus(Integer id, String status) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("UPDATE `case` SET `status` = ? WHERE `id` = ?");
        String[] columnNames = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        String[] values = {Update.REMAIN, Update.REMAIN, Update.REMAIN, Update.REMAIN, Update.REMAIN, Update.REMAIN, Update.REMAIN, status};
        Scan usersScanOperator = new Scan("case.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "id", Integer.toString(id));
        Update userUpdateOperator = new Update(selectOp, "case.sdb", columnNames, values);
    }

    public void modifyCaseCategory(String name, int id) throws SQLException {
        //pstmt2 = sqlConnection.prepareStatement("UPDATE `category_has_case` SET `category_name`= ? WHERE `case_id`= ?");
        String[] columnNames = {"category_name", "case_id"};
        String[] values = {name, Update.REMAIN};
        Scan usersScanOperator = new Scan("category_has_case.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "case_id", Integer.toString(id));
        Update userUpdateOperator = new Update(selectOp, "category_has_case.sdb", columnNames, values);
    }

    public void modifyCase(int id, String title, String date, String time, String description, String location, String category) throws SQLException, ParseException{
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date convertedDate = null;
//        Time convertedTime = null;
//        if(!date.isEmpty()) {
//            convertedDate = new Date(sdf.parse(date).getTime());
//        }
//        if(!time.isEmpty()) {
//            sdf = new SimpleDateFormat("HH:mm");
//            convertedTime = new Time(sdf.parse(time).getTime());
//        }
        //pstmt = sqlConnection.prepareStatement("UPDATE `case` SET `title`= ?, `date`= ?, `time`= ?, `description`= ?,`location`= ? WHERE `id`= ?");
        String[] columnNames1 = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        String[] values1 = {String.valueOf(id), title, date, time, description, location, Update.REMAIN, Update.REMAIN};
        Scan usersScanOperator1 = new Scan("case.sdb", columnNames1);
        Select<String> selectOp1 = new Select<String>(usersScanOperator1, "id", Integer.toString(id));
        Update userUpdateOperator1 = new Update(selectOp1, "case.sdb", columnNames1, values1);

        //pstmt2 = sqlConnection.prepareStatement("UPDATE `category_has_case` SET `category_name`= ? WHERE `case_id`= ?");
        String[] columnNames2 = {"category_name", "case_id"};
        String[] values2 = {category, String.valueOf(id)};
        Scan usersScanOperator2 = new Scan("category_has_case.sdb", columnNames2);
        Select<String> selectOp2 = new Select<String>(usersScanOperator2, "case_id", Integer.toString(id));
        Update userUpdateOperator2 = new Update(selectOp2, "category_has_case.sdb", columnNames2, values2);
    }

    public void modifyPersonOfInterest(int id, String firstName, String lastName, String dateOfBirth) throws SQLException, ParseException{
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date convertedDate = null;
//        if(!dateOfBirth.isEmpty()) {
//            convertedDate = new Date(sdf.parse(dateOfBirth).getTime());
//        }
        //pstmt = sqlConnection.prepareStatement("UPDATE `person_of_interest` SET `first_name`= ?, `last_name`= ?, `date_of_birth`= ? WHERE `id`= ?");
        String[] columnNames = {"id", "first_name", "last_name", "date_of_birth"};
        String[] values = {String.valueOf(id), firstName, lastName, dateOfBirth};
        Scan usersScanOperator = new Scan("person_of_interest.sdb", columnNames);
        Select<String> selectOp = new Select<String>(usersScanOperator, "id", Integer.toString(id));
        Update userUpdateOperator = new Update(selectOp, "person_of_interest.sdb", columnNames, values);
    }

    public void deleteCaseById(Integer id, Status status) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("DELETE FROM `case` WHERE id = ?");
        String[] columnNames = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        Delete userDeleteOperator = new Delete("case.sdb", columnNames);
        String values[] = {String.valueOf(id), Delete.ANY, Delete.ANY, Delete.ANY, Delete.ANY, Delete.ANY, Delete.ANY, status.getStatus()};
        Tuple tuple = new Tuple(new TupleSchema(columnNames), values);
        userDeleteOperator.delete(tuple);
    }

    public void deleteSubCategory(String name) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("DELETE FROM `category` WHERE name = ?");
        String[] columnNames = {"name", "parent"};
        Delete userDeleteOperator = new Delete("category.sdb", columnNames);
        String values[] = {name, Delete.ANY};
        Tuple tuple = new Tuple(new TupleSchema(columnNames), values);
        userDeleteOperator.delete(tuple);
    }

    public void deleteNoteById(int id) throws SQLException {
        //pstmt = sqlConnection.prepareStatement("DELETE FROM `note` WHERE id = ?");
        String[] columnNames = {"id", "text", "case_id", "person_of_interest_id", "user_name"};
        Delete userDeleteOperator = new Delete("note.sdb", columnNames);
        String values[] = {String.valueOf(id), Delete.ANY, Delete.ANY, Delete.ANY, Delete.ANY};
        Tuple tuple = new Tuple(new TupleSchema(columnNames), values);
        userDeleteOperator.delete(tuple);
    }

    public List<Case> searchByName(String name, String status) throws SQLException {
        List<Case> result = new ArrayList<Case>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM `case` WHERE  status LIKE ? AND (user_name LIKE ? OR description LIKE ? OR title LIKE ? OR id in(SELECT case_id FROM person_of_interest_has_case, person_of_interest WHERE person_of_interest_id = id AND (first_name LIKE ? OR last_name LIKE ?)))");
        String[] columnNames = {"id", "title", "date", "time", "description", "location", "user_name", "status"};
        Scan usersScanOperator = new Scan("case.sdb", columnNames);
        int[] likeColumns1= {0};
        Like likeOp1 = new Like(usersScanOperator, likeColumns1, status);
        int[] likeColumns2= {1, 4, 6};
        Like likeOp2 = new Like(likeOp1, likeColumns2, name);
        while(likeOp2.moveNext()) {
            result.add(new Case(likeOp2.current()));
        }
        return result;
    }

    public List<Case> getMostRecentCases() throws SQLException {
        List<Case> cases = getAllCases();
        List<Case> remove = new ArrayList<Case>();

        Collections.sort(cases, new Comparator<Case>() {
            public int compare(Case c1, Case c2) {
                if (c1.getDate() == null || c2.getDate() == null)
                    return 1;
                if (c1.getDate().equals(c2.getDate()))
                    return 0;
                else if (c1.getDate().before(c2.getDate()))
                    return 1;
                else
                    return -1;

            }
        });

        for(Case c : cases) {
            if(c.getDate() == null) {
                remove.add(c);
            }
        }
        cases.removeAll(remove);
        return cases;
    }

    public List<Case> getOldestUnsolvedCases() throws SQLException {
        List<Case> cases = getMostRecentCases();
        Collections.reverse(cases);
        List<Case> toBeRemoved = new ArrayList<Case>();
        for(Case c : cases) {
            if(c.getStatus().getStatus().equals("closed")) {
                toBeRemoved.add(c);
            }
        }
        cases.removeAll(toBeRemoved);
        return cases;
    }

    public List<PersonOfInterest> searchPoi(String name) throws SQLException {
        List<PersonOfInterest> result = new ArrayList<PersonOfInterest>();
        //pstmt = sqlConnection.prepareStatement("SELECT * FROM  `person_of_interest` WHERE `first_name` LIKE ? OR last_name LIKE ?");
        String[] columnNames = {"id", "first_name", "last_name", "date_of_birth"};
        Scan usersScanOperator = new Scan("person_of_interest.sdb", columnNames);
        int[] likeColumns= {1, 2};
        Like likeOp = new Like(usersScanOperator, likeColumns, name);
        while(likeOp.moveNext()) {
            result.add(new PersonOfInterest(likeOp.current()));
        }
        return result;
    }

    public List<Conviction> searchConviction(String name, String category, String date) throws SQLException {
        List<Conviction> convictions = getAllConvictions();
        List<Conviction> keep = new ArrayList<Conviction>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date convertedDate = null;
        try {
            if(!date.isEmpty()) {
                convertedDate = new Date(sdf.parse(date).getTime());
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        for(Conviction c : convictions) {
            if(c.getFirstName().contains(name) || c.getLastName().contains(name) || c.getTitle().contains(name)) {
                if(c.getCategory().contains(category)) {
                    if(date.isEmpty() || (c.getStartDate().before(convertedDate) && (c.getEndDate() == null || c.getEndDate().after(convertedDate)))){
                        keep.add(c);
                    }
                }
            }
        }

        return keep;
    }
}