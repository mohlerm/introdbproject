package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Object that represents a category of project (i.e. Theft, Assault...) 
 */
public final class Category {

	private String parent;
	private String name;
	private int count;

	public Category(ResultSet rs) throws SQLException {
		this.parent = rs.getString("parent");
		this.name = rs.getString("name");
	}

    public Category(Tuple tuple) {
        this.name = tuple.get(0);
        if(tuple.get(1).equals("NULL")) {
            this.parent = null;
        } else {
            this.parent = tuple.get(1);
        }
    }

	public final String getName() {
		return name;
	}
	
	public final String getParent() {
		return parent;
	}
	
	public void setCount(int i) {
		count = i;
	}
	
	public int getCount() {
		return count;
	}
	
	
}
