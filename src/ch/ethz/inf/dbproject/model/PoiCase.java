package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class PoiCase {
	private String firstName;
	private String lastName;
	private final int personId;
	private final int caseId;
	private final String title;
	private final String poiCategoryName;
	
	public PoiCase(final ResultSet rs) throws SQLException {
		this.firstName = rs.getString("first_name");
		this.lastName = rs.getString("last_name");
		this.personId = rs.getInt("person_id");
		this.caseId = rs.getInt("case_id");
		this.title = rs.getString("title");
		this.poiCategoryName = rs.getString("poi_category_name");
		
	}
    public PoiCase(Tuple tuple)  {
        this.firstName = tuple.get(13);
        this.lastName = tuple.get(14);
        this.personId = tuple.getInt(0);
        this.caseId = tuple.getInt(1);
        this.title = tuple.get(5);
        this.poiCategoryName = tuple.get(2);
    }

	public String getFirstName() {
		return firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public int getPersonId() {
		return personId;
	}

	public int getCaseId() {
		return caseId;
	}

	public String getTitle() {
		return title;
	}
	
	public String getPoiCategoryName() {
		return poiCategoryName;
	}


}