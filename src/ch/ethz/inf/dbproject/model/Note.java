package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

import java.sql.ResultSet;
import java.sql.SQLException;

public final class Note {

	private final int id;
	private final String text;
	private int personOfInterestId;
	private int caseId;
	private final String userName;
	
	public Note(ResultSet rs) throws SQLException {
		this.id = rs.getInt("id");
		this.text = rs.getString("text");
		// this is pretty ugly :P
		try {
			this.caseId = rs.getInt("person_of_interest_id");
		} catch (SQLException e) {
			this.caseId = -1;
		}
		try {
			this.personOfInterestId = rs.getInt("case_id");
		} catch (SQLException e) {
			this.personOfInterestId = -1;
		}
		this.userName = rs.getString("user_name");
	}
    public Note(Tuple tuple) {
        this.id = tuple.getInt(0);
        this.text = tuple.get(1);
        // this is pretty ugly :P
        try {
            this.caseId = tuple.getInt(2);
        } catch (Exception e) {
            this.caseId = -1;
        }
        try {
            this.personOfInterestId = tuple.getInt(3);
        } catch (Exception e) {
            this.personOfInterestId = -1;
        }
        this.userName = tuple.get(4);
    }
	
	public int getId() {
		return id;
	}

	public String getText() {
		return text;
	}
	
	public String getPartialText() {
		if(text.length() < 10) {
			return text;
		}
		return text.substring(0, 10)+"...";
	}
	
	public int getCaseId() {
		return caseId;
	}
	
	public int getPersonOfInterestId() {
		return personOfInterestId;
	}
	
	public String getUserName() {
		return userName;
	}
	

}
