package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Object that represents a conviction.
 */
public class Conviction {

	private final int convictionId;
	private final int caseId;
	private final String title;
	private final int personId;
	private final String firstName;
	private final String lastName;
	private final Date dateOfBirth;
	private final Date startDate;
	private final Date endDate;
	private final String category;
	
	public Conviction(ResultSet rs) throws SQLException {
		this.convictionId = rs.getInt("conviction_id");
		this.caseId = rs.getInt("case_id");
		this.title = rs.getString("title");
		this.firstName = rs.getString("first_name");
		this.lastName = rs.getString("last_name");
		this.dateOfBirth = rs.getDate("date_of_birth");
		this.personId = rs.getInt("person_of_interest_id");
		this.startDate = rs.getDate("start_date");
		this.endDate = rs.getDate("end_date");
		this.category = rs.getString("category_name");
	}
    public Conviction(Tuple tuple)  {
        this.convictionId = tuple.getInt(4);
        this.caseId = tuple.getInt(12);
        this.title = tuple.get(13);
        this.firstName = tuple.get(1);
        this.lastName = tuple.get(2);
        this.dateOfBirth = tuple.getDate(3);
        this.personId = tuple.getInt(0);
        this.startDate = tuple.getDate(5);
        this.endDate = tuple.getDate(6);
        this.category = tuple.get(10);
    }

	public int getConvictionId() {
		return convictionId;
	}
	
	public int getCaseId() {
		return caseId;
	}
	
	public String getTitle() {
		return title;
	}

	public int getPersonId() {
		return personId;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public Date getStartDate() {
		return startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public String getCategory() {
		return category;
	}
			
}
