package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Object that represents a registered in user.
 */
public final class Status {

	private final String status;

	public Status(final ResultSet rs) throws SQLException {
		this.status = rs.getString("status");
	}
    public Status(Tuple tuple) {
        this.status = tuple.get(0);
    }
	public Status(String string) throws SQLException {
		this.status = string;
	}

	public String getStatus() {
		return status;
	}
	
	public String toString() {
		return status;
	}

}
