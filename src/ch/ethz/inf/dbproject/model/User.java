package ch.ethz.inf.dbproject.model;

import ch.ethz.inf.dbproject.model.simpleDatabase.Tuple;
import ch.ethz.inf.dbproject.model.simpleDatabase.operators.AES;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Object that represents a registered in user.
 */
public final class User {

	private final String name;
	private final byte[] profilePicture;

	public User(ResultSet rs) throws SQLException {
		this.name = rs.getString("name");
		this.profilePicture = rs.getBytes("image");
	}
    public User(Tuple tuple) {
        this.name = tuple.get(0);
        if(tuple.get(2) != null && !tuple.get(2).isEmpty()) {
        	this.profilePicture = AES.hexStringToByteArray(tuple.get(2));
        }
        else {
            this.profilePicture = null;
        }
    }

	public String getName() {
		return name;
	}
	
	public String toString () {
		return name;
	}


	public byte[] getProfilePicture() {
		return profilePicture;
	}

}
