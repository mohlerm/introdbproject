package ch.ethz.inf.dbproject.model;

/**
 * Created by marcel on 5/21/14.
 * This is a Wrapper around the DatastoreInterface to enable usage of the Singleton pattern.
 * This isn't the most elegant way but by far the easiest solution while being able to use Interfaces.
 */
public class DatastoreInterfaceSingleton {
    private static DatastoreInterface instance = new DatastoreInterfaceSimpleDatabase();

    public static DatastoreInterface getInstance(){
        if(instance == null) {
            instance = new DatastoreInterfaceMySQL();
        }
        return instance;
    }
  }

