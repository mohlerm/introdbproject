package ch.ethz.inf.dbproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import ch.ethz.inf.dbproject.model.*;
import ch.ethz.inf.dbproject.util.UserManagement;


/**
 * Servlet implementation class of ModifyCase Page
 */
@WebServlet(description = "Page that allows to modify a case.", urlPatterns = { "/ModifyCase" })
public final class ModifyCaseServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = DatastoreInterfaceSingleton.getInstance();

	public final static String SESSION_USER_LOGGED_IN = "userLoggedIn";
	public final static String SESSION_USER_DETAILS = "userDetails";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ModifyCaseServlet() {
		super();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {
		final HttpSession session = request.getSession(true);
		final User loggedUser = UserManagement.getCurrentlyLoggedInUser(session);
		
		final String idString = request.getParameter("id");
		final Integer id = Integer.parseInt(idString);
		session.setAttribute("caseId", id);
		
		if (idString == null) {
			this.getServletContext().getRequestDispatcher("/Cases").forward(request, response);
		}
		
		try {
			final Case aCase = this.dbInterface.getCaseById(id);
			session.setAttribute("aCase", aCase);
			
			Category aCat = this.dbInterface.getCategoryByCaseId(id);
			session.setAttribute("aCat", aCat);
			
			List<Category> categories = this.dbInterface.getAllCategories();
			List<Category> categoryList = new ArrayList<Category>();
			for(Category category : categories) {
				if(category.getParent() == null) {
					categoryList.add(category);
					for(Category subcategory : categories) {
						if(subcategory.getParent() != null && subcategory.getParent().equals(category.getName())) {
							categoryList.add(subcategory);
						}
					}
				}
			}
			session.setAttribute("categoryList", categoryList);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		this.getServletContext().getRequestDispatcher("/ModifyCase.jsp").forward(request, response);
		
		
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doPost(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {

		final HttpSession session = request.getSession(true);
		final User loggedUser = UserManagement.getCurrentlyLoggedInUser(session);
		
		if (loggedUser == null) {
			// Not logged in!
			session.setAttribute(SESSION_USER_LOGGED_IN, false);
		} else {
			session.setAttribute(SESSION_USER_LOGGED_IN, true);
			// Logged in
			final String action = request.getParameter("action");
			if (action != null && action.trim().equals("modifyCase")) {
				final String title = request.getParameter("title");
				final String date = request.getParameter("date");
				final String time = request.getParameter("time");
				final String location = request.getParameter("location");
				final String description = request.getParameter("description");
				final String category = request.getParameter("category");
				try {
					dbInterface.modifyCase((int)session.getAttribute("caseId"), title, date, time, description, location, category);
					response.sendRedirect("Case?id="+Integer.toString((int)session.getAttribute("caseId")));
					//dbInterface.linkCategoryToId(category, id);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		

			}
	
		} 
	}
	
}
