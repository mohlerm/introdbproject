package ch.ethz.inf.dbproject;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.*;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.util.UserManagement;
import ch.ethz.inf.dbproject.util.html.BeanTableHelper;

/**
 * Servlet implementation class of Case Details Page
 */
@WebServlet(description = "Displays a specific case.", urlPatterns = { "/PersonOfInterest" })
public final class PersonOfInterestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = DatastoreInterfaceSingleton.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PersonOfInterestServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

		final HttpSession session = request.getSession(true);
		final User loggedUser = UserManagement.getCurrentlyLoggedInUser(session);

		final String idString = request.getParameter("id");
		if (idString == null) {
			this.getServletContext().getRequestDispatcher("/PersonsOfInterest").forward(request, response);
		}

		try {

			
			final String action = request.getParameter("action");
			if (action != null && action.trim().equals("deleteNote")) {
				try {
					this.dbInterface.deleteNoteById(Integer.parseInt(request.getParameter("noteId")));
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			
			final Integer id = Integer.parseInt(idString);
			final PersonOfInterest aPersonOfInterest = this.dbInterface.getPersonOfInterestById(id);

			
			/*******************************************************
			 * Construct a table to present all properties of a POI
			 *******************************************************/
			final BeanTableHelper<PersonOfInterest> table = new BeanTableHelper<PersonOfInterest>(
					"cases" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					PersonOfInterest.class 	/* The class of the objects (rows) that will be displayed */
			);

			// Add columns to the new table

			/*
			 * Column 1: The name of the item (This will probably have to be changed)
			 */
			
			table.addBeanColumn("First Name", "firstName");
			table.addBeanColumn("Last Name", "lastName");
			/*
			 * Columns 2 & 3: Some random fields. These should be replaced by i.e. funding progress, or time remaining
			 */	
			table.addBeanColumn("User ID", "id");
			table.addBeanColumn("Date of Birth", "dateOfBirth");

			table.addObject(aPersonOfInterest);
			table.setVertical(true);			

			session.setAttribute("personOfInterestTable", table);
			
			/*******************************************************
			 * Construct a table to present all notes of a case
			 *******************************************************/
			final BeanTableHelper<Note> notesTable = new BeanTableHelper<Note>(
					"cases" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					Note.class 	/* The class of the objects (rows) that will be displayed */
			);
			notesTable.addBeanColumn("Note", "partialText");
			notesTable.addBeanColumn("Username", "userName");
			notesTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"View full note" 	/* What should be displayed in every row */,
					"Note?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
			if(loggedUser != null) {
			notesTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"delete" 	/* What should be displayed in every row */,
					"PersonOfInterest?id=" + request.getParameter("id") +"&action=deleteNote&noteId=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
			}
			notesTable.setVertical(false);	
			notesTable.addObjects(this.dbInterface.getNotesByPersonOfInterest(Integer.parseInt(request.getParameter("id"))));
			
			session.setAttribute("notesTable", notesTable);
			
			/*******************************************************
			 * Construct a table to present all linked cases
			 *******************************************************/
			final BeanTableHelper<PoiCase> linkedCasesTable = new BeanTableHelper<PoiCase>(
					"cases" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					PoiCase.class 	/* The class of the objects (rows) that will be displayed */
			);

			// Add columns to the new table
			linkedCasesTable.addBeanColumn("Case Title", "title");
			linkedCasesTable.addBeanColumn("Category", "poiCategoryName");
			linkedCasesTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"View Case" 	/* What should be displayed in every row */,
					"Case?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"caseId" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);

			
			linkedCasesTable.addObjects(this.dbInterface.getAllCasesByPersonOfInterest(id));
			linkedCasesTable.setVertical(false);			

			session.setAttribute("linkedCasesTable", linkedCasesTable);
			
			/*******************************************************
			 * Construct a table to present all Convictions of a case
			 *******************************************************/
			final BeanTableHelper<Conviction> convictionsTable = new BeanTableHelper<Conviction>(
					"cases" 		/* The table html id property */,
					"casesTable" /* The table html class property */,
					Conviction.class 	/* The class of the objects (rows) that will be displayed */
			);
			convictionsTable.addBeanColumn("Case title", "title");
			convictionsTable.addBeanColumn("Start date", "startDate");
			convictionsTable.addBeanColumn("End date", "endDate");
			convictionsTable.addLinkColumn(""	/* The header. We will leave it empty */,
					"View Case" 	/* What should be displayed in every row */,
					"Case?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
					"caseId" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
	
			convictionsTable.setVertical(false);	

			convictionsTable.addObjects(this.dbInterface.getConvictionsByPersonId(id));
			session.setAttribute("convictionsTable", convictionsTable);
			
			
		} catch (final Exception ex) {
			ex.printStackTrace();
			this.getServletContext().getRequestDispatcher("/PersonsOfInterest.jsp").forward(request, response);
		}
		


		this.getServletContext().getRequestDispatcher("/PersonOfInterest.jsp").forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

		final HttpSession session = request.getSession(true);
		final User loggedUser = UserManagement.getCurrentlyLoggedInUser(session);

		final String action = request.getParameter("action");
		if (action != null && action.trim().equals("add_note") 	&& loggedUser != null) {
			// TODO: 
			try {
				dbInterface.insertNote(request.getParameter("note"), loggedUser.getName(), null, request.getParameter("id"));
				
				
				/*******************************************************
				 * Construct a table to present all notes of a case
				 *******************************************************/
				final BeanTableHelper<Note> notesTable = new BeanTableHelper<Note>(
						"cases" 		/* The table html id property */,
						"casesTable" /* The table html class property */,
						Note.class 	/* The class of the objects (rows) that will be displayed */
				);
				notesTable.addBeanColumn("Note", "partialText");
				notesTable.addBeanColumn("Username", "userName");
				notesTable.addLinkColumn(""	/* The header. We will leave it empty */,
						"View full note" 	/* What should be displayed in every row */,
						"Note?id=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
						"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
				if(loggedUser != null) {
					notesTable.addLinkColumn(""	/* The header. We will leave it empty */,
							"delete" 	/* What should be displayed in every row */,
							"PersonOfInterest?id=" + request.getParameter("id") +"&action=deleteNote&noteId=" 	/* This is the base url. The final url will be composed from the concatenation of this and the parameter below */, 
							"id" 			/* For every case displayed, the ID will be retrieved and will be attached to the url base above */);
					}
				notesTable.setVertical(false);	
				notesTable.addObjects(this.dbInterface.getNotesByCase(Integer.parseInt(request.getParameter("id"))));
				
				session.setAttribute("notesTable", notesTable);
				response.sendRedirect("PersonOfInterest?id="+ request.getParameter("id"));
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}