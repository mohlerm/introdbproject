package ch.ethz.inf.dbproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ch.ethz.inf.dbproject.model.*;
import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.util.UserManagement;

/**
 * Servlet implementation class of Case Details Page
 */
@WebServlet(description = "Displays a specific Note.", urlPatterns = { "/Note" })
public final class NoteServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = DatastoreInterfaceSingleton.getInstance();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public NoteServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {

		final HttpSession session = request.getSession(true);
		final User loggedUser = UserManagement.getCurrentlyLoggedInUser(session);

		final String idString = request.getParameter("id");
		if (idString == null) {
			this.getServletContext().getRequestDispatcher("/Note").forward(request, response);
		}

		try {

			final Integer id = Integer.parseInt(idString);
			final Note aNote = this.dbInterface.getNoteById(id);		

			session.setAttribute("note", aNote.getText());
			
			
		} catch (final Exception ex) {
			ex.printStackTrace();
			this.getServletContext().getRequestDispatcher("/Note.jsp").forward(request, response);
		}
		
		

		this.getServletContext().getRequestDispatcher("/Note.jsp").forward(request, response);
	}
}