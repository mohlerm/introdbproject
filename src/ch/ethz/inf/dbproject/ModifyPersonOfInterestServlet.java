package ch.ethz.inf.dbproject;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.sql.*;
import java.text.ParseException;

import ch.ethz.inf.dbproject.model.DatastoreInterface;
import ch.ethz.inf.dbproject.model.DatastoreInterfaceSingleton;
import ch.ethz.inf.dbproject.model.PersonOfInterest;
import ch.ethz.inf.dbproject.model.User;
import ch.ethz.inf.dbproject.util.UserManagement;


/**
 * Servlet implementation class of ModifyPersonOfInterest Page
 */
@WebServlet(description = "Page that allows to modify a case.", urlPatterns = { "/ModifyPersonOfInterest" })
public final class ModifyPersonOfInterestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private final DatastoreInterface dbInterface = DatastoreInterfaceSingleton.getInstance();

	public final static String SESSION_USER_LOGGED_IN = "userLoggedIn";
	public final static String SESSION_USER_DETAILS = "userDetails";

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ModifyPersonOfInterestServlet() {
		super();
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doGet(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {
		final HttpSession session = request.getSession(true);
		final User loggedUser = UserManagement.getCurrentlyLoggedInUser(session);
		
		final String idString = request.getParameter("id");
		final Integer id = Integer.parseInt(idString);
		session.setAttribute("id", id);
		
		if (idString == null) {
			this.getServletContext().getRequestDispatcher("/PersonsOfInterest").forward(request, response);
		}
		
		try {
			final PersonOfInterest aPoi = this.dbInterface.getPersonOfInterestById(id);
			session.setAttribute("aPoi", aPoi);
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		this.getServletContext().getRequestDispatcher("/ModifyPersonOfInterest.jsp").forward(request, response);
		
		
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected final void doPost(final HttpServletRequest request,
			final HttpServletResponse response) throws ServletException,
			IOException {

		final HttpSession session = request.getSession(true);
		final User loggedUser = UserManagement.getCurrentlyLoggedInUser(session);
		
		if (loggedUser == null) {
			// Not logged in!
			session.setAttribute(SESSION_USER_LOGGED_IN, false);
		} else {
			session.setAttribute(SESSION_USER_LOGGED_IN, true);
			// Logged in
			final String action = request.getParameter("action");
			if (action != null && action.trim().equals("modifyPersonOfInterest")) {
				final String firstName = request.getParameter("first_name");
				final String lastName = request.getParameter("last_name");
				final String dateOfBirth = request.getParameter("date_of_birth");
				try {
					dbInterface.modifyPersonOfInterest((int)session.getAttribute("id"), firstName, lastName, dateOfBirth);
					response.sendRedirect("PersonOfInterest?id="+Integer.toString((int)session.getAttribute("id")));
					//dbInterface.linkCategoryToId(category, id);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
		

			}
	
		} 
	}
	
}
